"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../lib/di");
const handler_1 = require("../lib/handler");
const route_1 = require("../lib/route");
const auth_controller_1 = require("../app/auth/auth.controller");
const case_controller_1 = require("../app/case/case.controller");
const file_controller_1 = require("../app/file/file.controller");
const note_controller_1 = require("../app/note/note.controller");
const user_controller_1 = require("../app/user/user.controller");
const event_controller_1 = require("../app/event/event.controller");
const order_controller_1 = require("../app/order/order.controller");
const study_controller_1 = require("../app/study/study.controller");
const report_controller_1 = require("../app/report/report.controller");
const patient_controller_1 = require("../app/patient/patient.controller");
const product_controller_1 = require("../app/product/product.controller");
const profile_controller_1 = require("../app/profile/profile.controller");
const request_controller_1 = require("../app/request/request.controller");
const setting_controller_1 = require("../app/setting/setting.controller");
const document_controller_1 = require("../app/document/document.controller");
const dashboard_controller_1 = require("../app/dashboard/dashboard.controller");
let Router = class Router {
    constructor(handler, 
        // 1
        auth, kase, file, note, user, event, order, study, report, patient, product, profile, request, setting, document, dashboard) {
        const instances = Array.from(arguments);
        const routes = route_1.create(instances.slice(1));
        this.middleware = route_1.compose([
            ...handler.all,
            awaitBody,
            auth.auth,
            ...routes,
        ]);
    }
    compose() {
        return this.middleware;
    }
};
Router = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [handler_1.Handler,
        auth_controller_1.AuthController,
        case_controller_1.CaseController,
        file_controller_1.FileController,
        note_controller_1.NoteController,
        user_controller_1.UserController,
        event_controller_1.EventController,
        order_controller_1.OrderController,
        study_controller_1.StudyController,
        report_controller_1.ReportController,
        patient_controller_1.PatientController,
        product_controller_1.ProductController,
        profile_controller_1.ProfileController,
        request_controller_1.RequestController,
        setting_controller_1.SettingController,
        document_controller_1.DocumentController,
        dashboard_controller_1.DashboardController])
], Router);
exports.Router = Router;
function awaitBody(context, next) {
    return __awaiter(this, void 0, void 0, function* () {
        yield next();
        if (context.body && context.body.then) {
            context.body = yield context.body;
        }
    });
}
