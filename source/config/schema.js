"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../lib/di");
const Sequelize_1 = require("../system/Sequelize");
const case_1 = require("../app/case/case");
const file_1 = require("../app/file/file");
const note_1 = require("../app/note/note");
const user_1 = require("../app/user/user");
const event_1 = require("../app/event/event");
const order_1 = require("../app/order/order");
const study_1 = require("../app/study/study");
const patient_1 = require("../app/patient/patient");
const product_1 = require("../app/product/product");
const document_1 = require("../app/document/document");
const stock_schema_1 = require("../persistence/product/stock.schema");
const transaction_schema_1 = require("../persistence/product/transaction.schema");
let Schema = class Schema {
    constructor(sequelize) {
        this.sequelize = sequelize;
        case_1.default(sequelize);
        file_1.default(sequelize);
        note_1.default(sequelize);
        user_1.default(sequelize);
        event_1.default(sequelize);
        order_1.default(sequelize);
        study_1.default(sequelize);
        patient_1.default(sequelize);
        product_1.default(sequelize);
        document_1.default(sequelize);
        stock_schema_1.default(sequelize);
        transaction_schema_1.default(sequelize);
    }
    sync() {
        const { sequelize } = this;
        const models = sequelize.models;
        for (const name in models) {
            if (models[name].associate) {
                models[name].associate(models);
            }
        }
        return this.sequelize.sync();
    }
};
Schema = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Sequelize_1.Sequelize])
], Schema);
exports.Schema = Schema;
