import { injectable } from '../lib/di';
import { Sequelize } from '../system/Sequelize';

import kase from '../app/case/case';
import file from '../app/file/file';
import note from '../app/note/note';
import user from '../app/user/user';

import event from '../app/event/event';
import order from '../app/order/order';
import study from '../app/study/study';

import patient from '../app/patient/patient';
import product from '../app/product/product';

import document from '../app/document/document';

import stock from '../persistence/product/stock.schema';
import transaction from '../persistence/product/transaction.schema';


@injectable
export class Schema {

	constructor(
		private sequelize: Sequelize
	) {
		kase(sequelize);
		file(sequelize);
		note(sequelize);
		user(sequelize);

		event(sequelize);
		order(sequelize);
		study(sequelize);

		patient(sequelize);
		product(sequelize);

		document(sequelize);

		stock(sequelize);
		transaction(sequelize);
	}


	sync() {

		const { sequelize } = this;

		const models = sequelize.models as any;

		for (const name in models) {
			if (models[name].associate) {
				models[name].associate(models);
			}
		}

		return this.sequelize.sync();

	}

}
