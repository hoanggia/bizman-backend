import { injectable } from '../lib/di';
import { Handler } from '../lib/handler';
import { compose, create } from '../lib/route';

import { AuthController } from '../app/auth/auth.controller';
import { CaseController } from '../app/case/case.controller';
import { FileController } from '../app/file/file.controller';
import { NoteController } from '../app/note/note.controller';
import { UserController } from '../app/user/user.controller';

import { EventController } from '../app/event/event.controller';
import { OrderController } from '../app/order/order.controller';
import { StudyController } from '../app/study/study.controller';

import { ReportController } from '../app/report/report.controller';

import { PatientController } from '../app/patient/patient.controller';
import { ProductController } from '../app/product/product.controller';
import { ProfileController } from '../app/profile/profile.controller';
import { RequestController } from '../app/request/request.controller';
import { SettingController } from '../app/setting/setting.controller';

import { DocumentController } from '../app/document/document.controller';

import { DashboardController } from '../app/dashboard/dashboard.controller';


@injectable
export class Router {

	private middleware;


	constructor(
		handler : Handler,
		// 1
		auth: AuthController,
		kase: CaseController,
		file: FileController,
		note: NoteController,
		user: UserController,

		event: EventController,
		order: OrderController,
		study: StudyController,

		report: ReportController,

		patient: PatientController,
		product: ProductController,
		profile: ProfileController,
		request: RequestController,
		setting: SettingController,

		document: DocumentController,

		dashboard: DashboardController
	) {

		const instances = Array.from(arguments);
		const routes = create(instances.slice(1));

		this.middleware = compose([
			...handler.all,
			awaitBody,
			auth.auth,
			...routes,
		]);

	}


	compose() {
		return this.middleware;
	}

}


async function awaitBody(context, next) {
	await next();
	if (context.body && context.body.then) {
		context.body = await context.body;
	}
}
