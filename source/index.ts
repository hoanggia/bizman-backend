import * as Koa from 'koa';

import { Config as Config1 } from './lib/config';
import { Config as Config2 } from './system/Config';

import { create, provide } from './lib/di';

import { Schema } from './config/schema';
import { Router } from './config/router';

import { Components } from './components/Components';
import { Router as MyRouter } from './system/Router';


export function start(config) {
	const injector = create([
		provide(Config1, { useValue: config }),
		provide(Config2, { useValue: config }),
	]);
	const schema = injector.get(Schema);
	const router = injector.get(Router);

	const components = injector.get(Components);
	const myRouter = injector.get(MyRouter) as MyRouter;
	const app = new Koa();
	app.use(router.compose());
	app.use(myRouter.middleware());
	console.log(schema);
	return schema.sync().then(() => app);

}
