"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../lib/di");
let EventBus = class EventBus {
    constructor() {
        this.listeners = new Map();
    }
    publish(event) {
        return __awaiter(this, void 0, void 0, function* () {
            const listeners = this.listeners.get(event.constructor) || [];
            for (const fn of listeners) {
                yield fn(event);
            }
        });
    }
    subscribe(type, listener) {
        const list = this.listeners.get(type) || [];
        list.push(listener);
        this.listeners.set(type, list);
    }
};
EventBus = __decorate([
    di_1.injectable
], EventBus);
exports.EventBus = EventBus;
