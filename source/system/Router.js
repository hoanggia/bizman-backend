"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../lib/di");
const route_1 = require("../lib/route");
let Router = class Router {
    constructor() {
        this.controllers = [];
    }
    register(controller) {
        this.controllers.push(controller);
    }
    middleware() {
        return route_1.compose(route_1.create(this.controllers));
    }
};
Router = __decorate([
    di_1.injectable
], Router);
exports.Router = Router;
