import { getNamespace } from 'continuation-local-storage';

import { injectable as autoInject } from '../lib/di';
import { Log as Logger } from '../lib/log';

import * as lodash from 'lodash';
import * as Origin from 'sequelize';

import { ModelDict } from '../persistence/ModelDict';
import { Config } from './Config';


type IModelDict = ModelDict;

export namespace Sequelize {
	export type DataType = Origin.DataTypes;
	export type ModelDict = IModelDict;
}


@autoInject
export class Sequelize extends Origin {

	readonly models: ModelDict;


	constructor(
		appConfig: Config,
		logger: Logger,
	) {
		(Origin as any).cls = getNamespace('bizman');

		const config = appConfig.sequelize;

		config.options.logging = true
			? logger.debug.bind(logger)
			: false;
		config.options.dialect = 'postgres';

		super(
			config.database,
			config.username,
			config.password,
			config.options
		);
	}


	/**
	 * Define a new model, representing a table in the database.
	 */
	define<TInstance, TAttrDict>(
		modelName: string,
		attrDict: AttrDict,
		options?: Origin.DefineOptions<TInstance>,
	): Origin.Model<TInstance, TAttrDict> {
		const attributes = lodash.cloneDeep(attrDict);
		return super.define<TInstance, TAttrDict>(modelName, attributes, options);
	}

}


// Restrict return type of `AttrDict` to make VS Code's auto completion
// works better.
interface AttrDict {
	readonly [name: string]: Origin.DefineAttributeColumnOptions;
}
