"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const continuation_local_storage_1 = require("continuation-local-storage");
const di_1 = require("../lib/di");
const log_1 = require("../lib/log");
const lodash = require("lodash");
const Origin = require("sequelize");
const Config_1 = require("./Config");
let Sequelize = class Sequelize extends Origin {
    constructor(appConfig, logger) {
        Origin.cls = continuation_local_storage_1.getNamespace('bizman');
        const config = appConfig.sequelize;
        config.options.logging = true
            ? logger.debug.bind(logger)
            : false;
        config.options.dialect = 'postgres';
        super(config.database, config.username, config.password, config.options);
    }
    /**
     * Define a new model, representing a table in the database.
     */
    define(modelName, attrDict, options) {
        const attributes = lodash.cloneDeep(attrDict);
        return super.define(modelName, attributes, options);
    }
};
Sequelize = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Config_1.Config,
        log_1.Log])
], Sequelize);
exports.Sequelize = Sequelize;
