import { injectable as autoInject } from '../lib/di';
import { create, compose } from '../lib/route';


@autoInject
export class Router {

	private readonly controllers = [];

	register(controller) {
		this.controllers.push(controller);
	}

	middleware() {
		return compose(create(this.controllers));
	}

}
