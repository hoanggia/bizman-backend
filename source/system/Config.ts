import * as Origin from 'sequelize';


export abstract class Config {

	readonly source: {
		readonly path: string;
	};

	readonly sequelize: {
		readonly database: string;
		readonly username: string;
		readonly password: string;
		readonly options : Origin.Options;
	};

}
