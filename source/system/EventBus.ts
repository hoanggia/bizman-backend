import { injectable as autoInject } from '../lib/di';


@autoInject
export class EventBus {

	private readonly listeners = new Map<any, Listener<any>[]>();


	async publish(event: Object) {
		const listeners = this.listeners.get(event.constructor) || [];
		for (const fn of listeners) {
			await fn(event);
		}
	}


	subscribe<T extends Object>(type: Type<T>, listener: Listener<T>) {
		const list = this.listeners.get(type) || [];
		list.push(listener);
		this.listeners.set(type, list);
	}

}


interface Type<T> {
	new(...args: any[]): T;
}

interface Listener<T> {
	(event: T): Promise<void>;
}
