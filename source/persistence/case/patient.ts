import { Instance, Model } from '../Model';


export interface PatientAttrDict {

	firstName: string;
	lastName: string;

	readonly fullName: string;

	dateOfBirth: number;
	medicareNo: string;

	address: any;
	phones: any;

}


export interface PatientInstance extends Instance<PatientAttrDict>, PatientAttrDict {
}


export interface PatientModel extends Model<PatientInstance, PatientAttrDict> {
}
