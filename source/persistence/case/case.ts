import { Instance, Model } from '../Model';

import { PatientInstance } from './patient';


export interface CaseAttrDict {
	readonly id: number;
	consultantId: number;
}


export interface CaseInstance extends Instance<CaseAttrDict>, CaseAttrDict {

	readonly patient?: PatientInstance;

}


export interface CaseModel extends Model<CaseInstance, CaseAttrDict> {
}
