import * as Origin from 'sequelize';


export interface Instance<TAttrDict> extends Origin.Instance<TAttrDict> {
}


export interface Model<TInstance, TAttrDict> extends Origin.Model<TInstance, TAttrDict> {

	/**
	 * Search for a single instance by its primary key.
	 */
	findById(id: number | string, options?: Origin.FindOptions): Promise<TInstance | null>;

}
