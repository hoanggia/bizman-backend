import { Instance, Model } from '../Model';


export interface UserAttrDict {

	readonly id: number;
	readonly role: string;

	firstName: string;
	lastName: string;

	readonly fullName: string;

	email?: string;
	hash ?: string;

	address?: string;
	address2?: any;
	phones?: any;
	data?: any;

}


export interface UserInstance extends Instance<UserAttrDict>, UserAttrDict {
}


export interface UserModel extends Model<UserInstance, UserAttrDict> {
}
