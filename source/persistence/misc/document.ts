import { Instance, Model } from '../Model';


export interface DocumentAttrDict {
}


export interface DocumentInstance extends Instance<DocumentAttrDict>, DocumentAttrDict {
}


export interface DocumentModel extends Model<DocumentInstance, DocumentAttrDict> {
}
