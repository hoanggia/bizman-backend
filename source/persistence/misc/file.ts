import { Instance, Model } from '../Model';


export interface FileAttrDict {
	readonly id: string;
	readonly name: string;
}


export interface FileInstance extends Instance<FileAttrDict>, FileAttrDict {
}


export interface FileModel extends Model<FileInstance, FileAttrDict> {
}
