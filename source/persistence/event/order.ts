import { Instance, Model } from '../Model';


export interface OrderAttrDict {
}


export interface OrderInstance extends Instance<OrderAttrDict>, OrderAttrDict {
}


export interface OrderModel extends Model<OrderInstance, OrderAttrDict> {
}
