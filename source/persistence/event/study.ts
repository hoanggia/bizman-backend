import { Instance, Model } from '../Model';


export interface StudyAttrDict {

	readonly id: number;
	readonly eventId: number;

	files: string[];
	status: string;
	readers: any[];
	studyDate: number;
	studyAppointment: any;
	completeDocumentFile: string[];

}


export interface StudyInstance extends Instance<StudyAttrDict>, StudyAttrDict {
}


export interface StudyModel extends Model<StudyInstance, StudyAttrDict> {
}
