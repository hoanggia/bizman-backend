import * as Origin from 'sequelize';

import { Instance, Model } from '../Model';

import { CaseInstance } from '../case/case';
import { UserInstance } from '../misc/user';


export interface EventAttrDict {

	readonly id: number;
	readonly date: number;
	readonly caseId: number;
	readonly type: string;

	referringDoctorId?: number;
	dentistId?: number;
	requestingSpecialistId?: number;
	reportingSpecialistId?: number;

	referringDoctor?: UserInstance;
	dentist?: UserInstance;
	requestingSpecialist?: UserInstance;
	reportingSpecialist?: UserInstance;
	createdById?: UserInstance;
	updatedById?: UserInstance;

	data: any;


}


export interface EventInstance extends Instance<EventAttrDict>, EventAttrDict {

	readonly case?: CaseInstance;

	readonly getCase: Origin.BelongsToGetAssociationMixin<CaseInstance>;

}


export interface EventModel extends Model<EventInstance, EventAttrDict> {
}
