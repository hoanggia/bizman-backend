import { Instance, Model } from '../Model';


export interface NoteAttrDict {
	readonly date: number;
}


export interface NoteInstance extends Instance<NoteAttrDict>, NoteAttrDict {
}


export interface NoteModel extends Model<NoteInstance, NoteAttrDict> {
}
