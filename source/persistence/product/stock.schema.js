"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize_1 = require("../../system/Sequelize");
function default_1(sequelize) {
    const DataType = Sequelize_1.Sequelize;
    sequelize.define('stock', {
        quantity: {
            type: DataType.INTEGER,
            allowNull: false,
        },
    }, {
        classMethods: { associate },
        timestamps: false,
    });
}
exports.default = default_1;
function associate(models) {
    models.user.hasMany(models.stock, {
        foreignKey: { allowNull: false, name: 'consultantId' },
    });
    models.product.hasMany(models.stock, {
        foreignKey: { allowNull: false },
    });
}
