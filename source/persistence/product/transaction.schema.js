"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize_1 = require("../../system/Sequelize");
function default_1(sequelize) {
    const DataType = Sequelize_1.Sequelize;
    sequelize.define('transaction', {
        date: {
            type: DataType.DATE,
            allowNull: false,
        },
        quantity: {
            type: DataType.INTEGER,
            allowNull: false,
        },
    }, {
        classMethods: { associate },
        timestamps: false,
    });
}
exports.default = default_1;
function associate(models) {
    const { transaction } = models;
    transaction.belongsTo(models.user, {
        as: 'consultant',
        foreignKey: { allowNull: false },
        onDelete: 'restrict',
    });
    transaction.belongsTo(models.product, {
        foreignKey: { allowNull: false },
        onDelete: 'restrict',
    });
    transaction.belongsTo(models.user, {
        as: 'actor',
        onDelete: 'restrict',
    });
    transaction.belongsTo(models.event, {
        onDelete: 'restrict',
    });
}
