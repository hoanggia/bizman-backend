import { Sequelize } from '../../system/Sequelize';


export default function(sequelize: Sequelize) {

	const DataType: Sequelize.DataType = Sequelize;

	sequelize.define('stock', {

		quantity: {
			type: DataType.INTEGER,
			allowNull: false,
		},

	}, {

		classMethods: { associate },

		timestamps: false,

	});

}


function associate(models: Sequelize.ModelDict) {

	models.user.hasMany(models.stock, {
		foreignKey: { allowNull: false, name: 'consultantId' },
	});

	models.product.hasMany(models.stock, {
		foreignKey: { allowNull: false },
	});

}
