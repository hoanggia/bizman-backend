import { Instance, Model } from '../Model';

import { EventInstance } from '../event/event';
import { UserInstance } from '../misc/user';


export interface TransactionAttrDict {

	readonly id: number;
	readonly date: Date;
	readonly quantity: number;

	readonly consultantId: number;
	readonly productId: number;

	readonly actorId?: number;
	readonly eventId?: number;

}


export interface TransactionInstance extends Instance<TransactionAttrDict>, TransactionAttrDict {
	readonly actor?: UserInstance;
	readonly event?: EventInstance;
}


export interface TransactionModel extends Model<TransactionInstance, TransactionAttrDict> {
}
