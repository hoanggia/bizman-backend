import { Instance, Model } from '../Model';


export interface StockAttrDict {

	readonly consultantId: number;
	readonly productId: number;

	quantity: number;

}


export interface StockInstance extends Instance<StockAttrDict>, StockAttrDict {
}


export interface StockModel extends Model<StockInstance, StockAttrDict> {
}
