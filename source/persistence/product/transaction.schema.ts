import { Sequelize } from '../../system/Sequelize';


export default function(sequelize: Sequelize) {

	const DataType: Sequelize.DataType = Sequelize;

	sequelize.define('transaction', {

		date: {
			type: DataType.DATE,
			allowNull: false,
		},

		quantity: {
			type: DataType.INTEGER,
			allowNull: false,
		},

	}, {

		classMethods: { associate },

		timestamps: false,

	});

}


function associate(models: Sequelize.ModelDict) {

	const { transaction } = models;

	transaction.belongsTo(models.user, {
		as: 'consultant',
		foreignKey: { allowNull: false },
		onDelete: 'restrict',
	});

	transaction.belongsTo(models.product, {
		foreignKey: { allowNull: false },
		onDelete: 'restrict',
	});

	transaction.belongsTo(models.user, {
		as: 'actor',
		onDelete: 'restrict',
	});

	transaction.belongsTo(models.event, {
		onDelete: 'restrict',
	});

}
