import { Instance, Model } from '../Model';


export interface ProductAttrDict {
}


export interface ProductInstance extends Instance<ProductAttrDict>, ProductAttrDict {
}


export interface ProductModel extends Model<ProductInstance, ProductAttrDict> {
}
