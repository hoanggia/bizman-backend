import * as Origin from 'sequelize';

import { CaseModel } from './case/case';
import { PatientModel } from './case/patient';

import { EventModel } from './event/event';
import { OrderModel } from './event/order';
import { StudyModel } from './event/study';

import { NoteModel } from './event/note';

import { DocumentModel } from './misc/document';
import { FileModel } from './misc/file';
import { UserModel } from './misc/user';

import { ProductModel } from './product/product';
import { StockModel } from './product/stock';
import { TransactionModel } from './product/transaction';


export interface ModelDict {

	readonly case: CaseModel;
	readonly patient: PatientModel;

	readonly event: EventModel;
	readonly order: OrderModel;
	readonly study: StudyModel;

	readonly note: NoteModel;

	readonly document: DocumentModel;
	readonly file: FileModel;
	readonly user: UserModel;

	readonly product: ProductModel;
	readonly stock: StockModel;
	readonly transaction: TransactionModel;

	// To be compatible with `Origin.ModelsHashInterface`
	readonly [name: string]: Origin.Model<any, any>;

}
