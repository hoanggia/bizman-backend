export const NoteTypeList = {
	Call: 'call',
	File: 'file',
	Note: 'note',
};


export const NoteTypeLabel = {
	[NoteTypeList.Call]: 'Follow-up Phone Call',
	[NoteTypeList.File]: 'Follow-up Download',
	[NoteTypeList.Note]: 'General Note',
};
