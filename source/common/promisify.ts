export function promisify(fn: Function) {

	return function(...args) {
		const self = this;
		return new Promise(function(resolve, reject) {
			fn.call(self, ...args, function(err, ...values) {
				if (err) {
					reject(err);
				} else {
					const data = values.length === 1 ? values[0] : values;
					resolve(data);
				}
			});
		});
	};

}
