"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleList = {
    Admin: 'admin',
    Consultant: 'consultant',
    ReferringDoctor: 'referring-doctor',
    SleepSpecialist: 'sleep-specialist',
    SleepAnalyst: 'sleep-analyst',
    SleepScientist: 'sleep-scientist',
};
exports.RoleLabel = {
    [exports.RoleList.Admin]: 'Admin',
    [exports.RoleList.Consultant]: 'Consultant',
    [exports.RoleList.ReferringDoctor]: 'Referring Dr.',
    [exports.RoleList.SleepSpecialist]: 'Sleep Specialist',
    [exports.RoleList.SleepAnalyst]: 'Sleep Analyst',
    [exports.RoleList.SleepScientist]: 'Sleep Scientist',
};
