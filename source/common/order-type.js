"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeList = {
    Byod: 'byod',
    Hire: 'hire',
    Sale: 'sale',
};
exports.TypeLabel = {
    [exports.TypeList.Hire]: 'CPAP Trial (Hire & Follow Up)',
    [exports.TypeList.Sale]: 'Sale & Follow Up (after Trial)',
    [exports.TypeList.Byod]: 'Sale & Follow Up (without Trial)',
};
