"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoteTypeList = {
    Call: 'call',
    File: 'file',
    Note: 'note',
};
exports.NoteTypeLabel = {
    [exports.NoteTypeList.Call]: 'Follow-up Phone Call',
    [exports.NoteTypeList.File]: 'Follow-up Download',
    [exports.NoteTypeList.Note]: 'General Note',
};
