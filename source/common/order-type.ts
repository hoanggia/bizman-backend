export const TypeList = {
	Byod: 'byod',
	Hire: 'hire',
	Sale: 'sale',
};


export const TypeLabel = {
	[TypeList.Hire]: 'CPAP Trial (Hire & Follow Up)',
	[TypeList.Sale]: 'Sale & Follow Up (after Trial)',
	[TypeList.Byod]: 'Sale & Follow Up (without Trial)',
};
