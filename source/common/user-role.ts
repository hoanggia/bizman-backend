export const RoleList = {
	Admin: 'admin',
	Consultant: 'consultant',
	ReferringDoctor: 'referring-doctor',
	SleepSpecialist: 'sleep-specialist',
	SleepAnalyst: 'sleep-analyst',
	SleepScientist: 'sleep-scientist',
};


export const RoleLabel = {
	[RoleList.Admin]: 'Admin',
	[RoleList.Consultant]: 'Consultant',
	[RoleList.ReferringDoctor]: 'Referring Dr.',
	[RoleList.SleepSpecialist]: 'Sleep Specialist',
	[RoleList.SleepAnalyst]: 'Sleep Analyst',
	[RoleList.SleepScientist]: 'Sleep Scientist',
};
