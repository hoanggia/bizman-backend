import { injectable as autoInject } from '../lib/di';

import { FormComponent } from './form_component';
import { TransactionCommandSide } from './transaction_command_side';
import { TransactionQuerySide } from './transaction_query_side';

import { EventComponent } from './event_command_side';


@autoInject
export class Components {

	constructor(
		form_component: FormComponent,
		transaction_command_side: TransactionCommandSide,
		transaction_query_side: TransactionQuerySide,
		event_command_side: EventComponent,
	) {}

}
