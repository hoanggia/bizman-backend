"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../lib/di");
const FormController_1 = require("./delivery/FormController");
let FormComponent = class FormComponent {
    constructor(controller) { }
};
FormComponent = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [FormController_1.FormController])
], FormComponent);
exports.FormComponent = FormComponent;
