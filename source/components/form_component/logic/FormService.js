"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const moment = require("moment");
const di_1 = require("../../../lib/di");
const errors_1 = require("../../../lib/errors");
const PdfFiller_1 = require("../../../lib/PdfFiller");
const Config_1 = require("../../../system/Config");
const Sequelize_1 = require("../../../system/Sequelize");
let FormService = class FormService {
    constructor(sequelize, config) {
        this.sequelize = sequelize;
        const baseDir = path.join(config.source.path, 'components/form_component/logic');
        this.studyReferralPdf = path.join(baseDir, 'study-referral.pdf');
        this.techSheetPdf = path.join(baseDir, 'tech-sheet.pdf');
        this.consentForDiscosurePdf = path.join(baseDir, 'consent-for-disclosure.pdf');
    }
    fillStudy(eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const include = [
                { model: models.case, include: [models.patient] },
            ];
            const event = yield models.event.findById(eventId, { include });
            if (!event)
                throw new errors_1.NotFound(eventId);
            const { data } = event;
            const patient = event.case.patient;
            const consultantId = event.case.consultantId;
            const user = yield models.user.findById(consultantId);
            const fields = {
                'Do you snore loudly?': data.stop.snore ? 'Yes' : 'Off',
                'Do you often feel tired, fatigued or sleepy during the daytime?': data.stop.tired ? 'Yes' : 'Off',
                'Has anyone noticed you stop breathing during your sleep?': data.stop.breathing ? 'Yes' : 'Off',
                'Do you have or are you being treated for high blood pressure?': data.stop.high ? 'Yes' : 'Off',
                'Date': moment.utc(Date.now()).format('DD/MM/YYYY'),
                'Patient First Name, Last Name': patient.fullName,
                'Patient Address': patient.address.line,
                'Date of Birth': moment.unix(patient.dateOfBirth).format('DD/MM/YYYY'),
                'Medicare No': patient.medicareNo || '',
                'Mobile': patient.phones ? patient.phones.mobile : '',
                'Home Work No': patient.phones ? (patient.phones.home || patient.phones.work || '') : '',
                'Height': data.clinical.height,
                'Weight': data.clinical.weight,
                'Neck Circumference': data.clinical.neck || '',
                'Home Based Sleep Study  patient sleeps in the comfort of their own home using a portable sleep dia gnostic device': data.services.diagnostic ? 'On' : 'Off',
                'Hospital Based Sleep Study  to investigate for more complex sleep disorders including Insomnia Narcolepsy': data.services.investigation ? 'On' : 'Off',
                'Consultation with Sleep Physician  patient will be reviewed by a Sleep Physician on the CLM Sleep panel': data.services.consultation ? 'On' : 'Off',
                'Reason for investigation': data.clinical.reason || '',
                'Driving License Assessment Heavy vehicle or Light vehicle please circle': data.clinical.driving ? 'On' : 'Off',
                'Heavy Vechile': data.clinical.heavy ? 'On' : 'Off',
                'Light Vechicle': data.clinical.light ? 'On' : 'Off',
                'Sitting and reading': data.ess.reading,
                'Watching television': data.ess.television,
                'Sitting inactive in a public place eg a theatre or meeting': data.ess.public,
                'As a passenger in a car for an hour without a break': data.ess.passenger,
                'Lying down to rest in the afternoon when circumstances permit': data.ess.rest,
                'Sitting and talking to someone': data.ess.talking,
                'Sitting quietly after a lunch without alcohol': data.ess.lunch,
                'In a car while stopped for a few minutes in the traffic': data.ess.traffic,
                'TOTAL SCORE': data.score,
                'Suburb': user.address2 ? (user.address2.suburb || '') : '',
                'Postcode': user.address2 ? (user.address2.postcode || '') : '',
                'State': user.address2 ? (user.address2.state || '') : '',
                'User First Name, Last Name': user.fullName,
                'Address': user.address || '',
                'Address1': user.address2 ? (user.address2.address || '') : '',
                'Telephone': user.phones ? (user.phones.work || user.phones.home || user.phones.mobile || '') : '',
                'Fax': user.phones ? (user.phones.fax || '') : '',
                'Email User': user.email,
            };
            const details = data.clinical.details || '';
            const lines = details.split('\n');
            for (let i = 0; i < lines.length; i++) {
                fields['Medication' + (i + 1)] = lines[i];
            }
            const destPath = getPdfPath(eventId, 'study-referral');
            yield PdfFiller_1.fillForm(this.studyReferralPdf, destPath, fields);
            return destPath;
        });
    }
    fillTechSheet(eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const include = [
                { model: models.case, include: [models.patient] },
                { model: models.user, as: 'referringDoctor' },
                { model: models.user, as: 'reportingSpecialist' },
                { model: models.study },
            ];
            const event = yield models.event.findById(eventId, { include });
            if (!event)
                throw new errors_1.NotFound(eventId);
            const study = yield models.study.findAll({ where: { eventId: eventId } });
            const doctor = event.referringDoctor;
            const specialist = event.reportingSpecialist;
            const consultantId = event.case.consultantId;
            const consultant = yield models.user.findById(consultantId);
            const patient = event.case.patient;
            const { medicareNo } = patient;
            const isoRefDate = moment.unix(event.date).format('DDMMYY');
            const dateOfBirth = moment.unix(patient.dateOfBirth).format('DD/MM/YYYY');
            const dateOfappointment = (study[0].studyAppointment ? moment.unix(study[0].studyAppointment.appointmentDate).format('DDMMYY') || '' : '');
            const fields = {
                'Patient First Name, Last Name': patient.fullName,
                'Reporting Sleep Physician': specialist.fullName,
                'Firstname': patient.firstName,
                'Surname': patient.lastName,
                'Add1': patient.address.line,
                'Add2': ' ' + (patient.address.suburb ? patient.address.suburb : '') + ' ' + (patient.address.postcode ? patient.address.postcode : '') + ' ' + (patient.address.state ? patient.address.state : ''),
                'DOB': dateOfBirth,
                'Address': consultant.address ? (consultant.address) : '',
                'Address1': consultant.address2 ? (consultant.address2.address || '') : '',
                'Suburb': consultant.address2 ? (consultant.address2.suburb || '') : '',
                'Postcode': consultant.address2 ? (consultant.address2.postcode || '') : '',
                'State': consultant.address2 ? (consultant.address2.state || '') : '',
                'Telephone': consultant.phones ? (consultant.phones.work || consultant.phones.home || consultant.phones.mobile || '') : '',
                'Fax': consultant.phones ? (consultant.phones.fax || '') : '',
                'Email User': consultant.email,
                'User First Name, Last Name': consultant.fullName,
                '2.DD1': isoRefDate[0],
                '2.DD2': isoRefDate[1],
                '2.MM1': isoRefDate[2],
                '2.MM2': isoRefDate[3],
                '2.YY1': isoRefDate[4],
                '2.YY2': isoRefDate[5],
                'DD1': (dateOfappointment.length > 1 ? dateOfappointment[0] : ''),
                'DD2': (dateOfappointment.length > 1 ? dateOfappointment[1] : ''),
                'MM1': (dateOfappointment.length > 1 ? dateOfappointment[2] : ''),
                'MM2': (dateOfappointment.length > 1 ? dateOfappointment[3] : ''),
                'YY1': (dateOfappointment.length > 1 ? dateOfappointment[4] : ''),
                'YY2': (dateOfappointment.length > 1 ? dateOfappointment[5] : ''),
                'Ref': medicareNo[medicareNo.length - 1],
                'Provider': specialist.fullName + ' ' + (specialist.data ? (specialist.data.providerNo || '') : '')
                    + (specialist.address ? '\n' + specialist.address : '')
                    + '\n' + (specialist.address2 ? (specialist.address2.address || '') : '')
                    + ' ' + (specialist.address2 ? (specialist.address2.suburb || '') : '')
                    + ' ' + (specialist.address2 ? (specialist.address2.postcode || '') : '')
                    + ' ' + (specialist.address2 ? (specialist.address2.state || '') : ''),
                'Technician Name': study[0].studyAppointment ? (study[0].studyAppointment.technician || '') : '',
                'Study Date': study[0].studyDate ? (moment.unix(study[0].studyDate).format('DD/MM/YYYY') || '') : '',
                'PDx Unit': study[0].studyAppointment ? (study[0].studyAppointment.pdx || '') : '',
            };
            if (doctor) {
                fields['RefPrac'] = doctor.fullName + (doctor.address ? '\n' + doctor.address : '')
                    + '\n' + (doctor.address2 ? (doctor.address2.address || '') : '')
                    + ' ' + (doctor.address2 ? (doctor.address2.suburb || '') : '')
                    + ' ' + (doctor.address2 ? (doctor.address2.postcode || '') : '')
                    + ' ' + (doctor.address2 ? (doctor.address2.state || '') : '');
            }
            const { providerNo } = doctor ? doctor.data : {};
            if (providerNo) {
                for (let i = 0; i < providerNo.length; i++) {
                    fields['PN' + (i + 1)] = providerNo[i];
                }
            }
            if (medicareNo) {
                for (let i = 0; i < medicareNo.length; i++) {
                    fields['MA' + (i + 1)] = medicareNo[i];
                }
            }
            const destPath = getPdfPath(eventId, 'tech-sheet');
            yield PdfFiller_1.fillForm(this.techSheetPdf, destPath, fields);
            return destPath;
        });
    }
    fillConsentForDisclosure(caseId) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const caseData = yield models.case.findById(caseId);
            if (!caseData)
                throw new errors_1.NotFound(caseId);
            const patient = caseData.patient;
            const consultantId = caseData.consultantId;
            const user = yield models.user.findById(consultantId);
            const fields = {
                'Patient First Name, Last Name': patient.fullName,
                'User First Name, Last Name': user.fullName,
                'Suburb': user.address2 ? (user.address2.suburb || '') : '',
                'Postcode': user.address2 ? (user.address2.postcode || '') : '',
                'State': user.address2 ? (user.address2.state || '') : '',
                'Address': user.address || '',
                'Address1': user.address2 ? (user.address2.address || '') : '',
                'Telephone': user.phones ? (user.phones.work || user.phones.home || user.phones.mobile || '') : '',
                'Fax': user.phones ? (user.phones.fax || '') : '',
                'Email User': user.email,
                'Date': moment.utc(Date.now()).format('DD/MM/YYYY'),
            };
            const destPath = getProfilePdfPath(caseId, 'consent-for-disclosure');
            yield PdfFiller_1.fillForm(this.consentForDiscosurePdf, destPath, fields);
            return destPath;
        });
    }
};
FormService = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Sequelize_1.Sequelize,
        Config_1.Config])
], FormService);
exports.FormService = FormService;
function getPdfPath(eventId, pdfName) {
    const dataDir = process.env.DATA_DIR;
    return path.join(dataDir, 'pdf', `${pdfName}-${eventId}.pdf`);
}
function getProfilePdfPath(caseId, pdfName) {
    const dataDir = process.env.DATA_DIR;
    return path.join(dataDir, 'pdf', `${pdfName}-${caseId}.pdf`);
}
