"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const di_1 = require("../../../lib/di");
const lib_1 = require("../../../app/lib");
const Router_1 = require("../../../system/Router");
const FormService_1 = require("../logic/FormService");
let FormController = class FormController {
    constructor(service, router) {
        this.service = service;
        router.register(this);
    }
    fillStudy(context, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const filePath = yield this.service.fillStudy(+eventId);
            context.body = fs.createReadStream(filePath);
        });
    }
    fillTechSheet(context, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const filePath = yield this.service.fillTechSheet(+eventId);
            context.body = fs.createReadStream(filePath);
        });
    }
    fillConsentForDisclosure(context, caseId) {
        return __awaiter(this, void 0, void 0, function* () {
            const filePath = yield this.service.fillConsentForDisclosure(+caseId);
            context.body = fs.createReadStream(filePath);
        });
    }
};
__decorate([
    lib_1.route.post('/forms/:eventId/study-referral'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FormController.prototype, "fillStudy", null);
__decorate([
    lib_1.route.post('/forms/:eventId/tech-sheet'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FormController.prototype, "fillTechSheet", null);
__decorate([
    lib_1.route.post('/forms/:caseId/consent-for-disclosure'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FormController.prototype, "fillConsentForDisclosure", null);
FormController = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [FormService_1.FormService,
        Router_1.Router])
], FormController);
exports.FormController = FormController;
