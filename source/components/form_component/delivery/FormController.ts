import * as fs from 'fs';

import { injectable as autoInject } from '../../../lib/di';
import { route as Http, Context } from '../../../app/lib';
import { Router } from '../../../system/Router';

import { FormService } from '../logic/FormService';


@autoInject
export class FormController {

	constructor(
		private readonly service: FormService,
		router: Router,
	) {
		router.register(this);
	}


	@Http.post('/forms/:eventId/study-referral')

	private async fillStudy(context: Context, eventId: string) {
		const filePath = await this.service.fillStudy(+eventId);
		context.body = fs.createReadStream(filePath);
	}


	@Http.post('/forms/:eventId/tech-sheet')

	private async fillTechSheet(context: Context, eventId: string) {
		const filePath = await this.service.fillTechSheet(+eventId);
		context.body = fs.createReadStream(filePath);
	}

	@Http.post('/forms/:caseId/consent-for-disclosure')
	
	private async fillConsentForDisclosure(context: Context, caseId: string){
		const filePath = await this.service.fillConsentForDisclosure(+caseId);
		context.body = fs.createReadStream(filePath);
	}

}
