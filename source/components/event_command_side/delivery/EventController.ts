import { injectable as autoInject } from '../../../lib/di';
import { route as Http, Context, parse } from '../../../app/lib';
import { Router } from '../../../system/Router';

import { EventService } from '../logic/EventService';

@autoInject
export class EventController {

	constructor(
		private readonly service: EventService,
		router: Router,
	) {
		router.register(this);
	}


	@Http.post('/events/:eventId/study-options')

	private async saveStudyOptions(context: Context, eventId: string) {

		const data = await parse(context);

		await this.service.saveStudyOptions(+eventId, data);
		context.status = 204; // No Content

	}

}
