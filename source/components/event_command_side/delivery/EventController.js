"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../../lib/di");
const lib_1 = require("../../../app/lib");
const Router_1 = require("../../../system/Router");
const EventService_1 = require("../logic/EventService");
let EventController = class EventController {
    constructor(service, router) {
        this.service = service;
        router.register(this);
    }
    saveStudyOptions(context, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield lib_1.parse(context);
            yield this.service.saveStudyOptions(+eventId, data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.route.post('/events/:eventId/study-options'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "saveStudyOptions", null);
EventController = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [EventService_1.EventService,
        Router_1.Router])
], EventController);
exports.EventController = EventController;
