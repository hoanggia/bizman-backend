import { injectable as autoInject } from '../../lib/di';

import { EventController } from './delivery/EventController';

@autoInject
export class EventComponent {

	constructor(
		controller: EventController,
	) {}

}