"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../lib/di");
const EventController_1 = require("./delivery/EventController");
let EventComponent = class EventComponent {
    constructor(controller) { }
};
EventComponent = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [EventController_1.EventController])
], EventComponent);
exports.EventComponent = EventComponent;
