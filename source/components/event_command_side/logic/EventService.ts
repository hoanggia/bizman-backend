import { injectable as autoInject } from '../../../lib/di';


import { NotFound, State } from '../../../app/lib';
import { Sequelize } from '../../../system/Sequelize';


@autoInject
export class EventService  {

	constructor(
		private readonly sequelize: Sequelize,
	) {}


	async saveStudyOptions(eventId: number, data) {

		const { models } = this.sequelize;
		const item = await models.event.findById(eventId);

		if (!item) throw new NotFound(eventId);

		item.data = Object.assign({}, item.data, data);
		
		await item.save();

	}

}
