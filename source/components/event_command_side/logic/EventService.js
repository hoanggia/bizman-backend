"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../../lib/di");
const lib_1 = require("../../../app/lib");
const Sequelize_1 = require("../../../system/Sequelize");
let EventService = class EventService {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }
    saveStudyOptions(eventId, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const item = yield models.event.findById(eventId);
            if (!item)
                throw new lib_1.NotFound(eventId);
            item.data = Object.assign({}, item.data, data);
            yield item.save();
        });
    }
};
EventService = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Sequelize_1.Sequelize])
], EventService);
exports.EventService = EventService;
