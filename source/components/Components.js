"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../lib/di");
const form_component_1 = require("./form_component");
const transaction_command_side_1 = require("./transaction_command_side");
const transaction_query_side_1 = require("./transaction_query_side");
const event_command_side_1 = require("./event_command_side");
let Components = class Components {
    constructor(form_component, transaction_command_side, transaction_query_side, event_command_side) { }
};
Components = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [form_component_1.FormComponent,
        transaction_command_side_1.TransactionCommandSide,
        transaction_query_side_1.TransactionQuerySide,
        event_command_side_1.EventComponent])
], Components);
exports.Components = Components;
