import { injectable as autoInject } from '../../lib/di';

import { TransactionHandler } from './logic/TransactionHandler';


@autoInject
export class TransactionCommandSide {

	constructor(
		handler: TransactionHandler,
	) {}

}
