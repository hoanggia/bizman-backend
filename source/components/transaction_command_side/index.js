"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../lib/di");
const TransactionHandler_1 = require("./logic/TransactionHandler");
let TransactionCommandSide = class TransactionCommandSide {
    constructor(handler) { }
};
TransactionCommandSide = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [TransactionHandler_1.TransactionHandler])
], TransactionCommandSide);
exports.TransactionCommandSide = TransactionCommandSide;
