"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../../lib/di");
const EventBus_1 = require("../../../system/EventBus");
const Sequelize_1 = require("../../../system/Sequelize");
const EventCreated_1 = require("../../../app/event/EventCreated");
const EventUpdated_1 = require("../../../app/event/EventUpdated");
let TransactionHandler = class TransactionHandler {
    constructor(sequelize, bus) {
        this.sequelize = sequelize;
        bus.subscribe(EventCreated_1.EventCreated, event => this.onCreated(event));
        bus.subscribe(EventUpdated_1.EventUpdated, event => this.onUpdated(event));
    }
    onCreated(message) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { event } = message;
            if (!event.data.devices)
                return;
            const devices = event.data.devices.concat(event.data.ownDevices);
            const quantityMap = {};
            for (const device of devices) {
                const { productId } = device;
                if (quantityMap[productId]) {
                    quantityMap[productId]++;
                }
                else {
                    quantityMap[productId] = 1;
                }
            }
            const aCase = yield event.getCase();
            const { consultantId } = aCase;
            for (const productId in quantityMap) {
                const stock = yield models.stock.findOne({
                    where: { consultantId, productId },
                });
                let quantity = stock ? stock.quantity : 0;
                quantity = quantity - quantityMap[productId];
                yield models.stock.upsert({
                    quantity,
                    productId,
                    consultantId,
                });
                yield models.transaction.create({
                    date: new Date,
                    quantity: -quantityMap[productId],
                    consultantId,
                    productId: +productId,
                    actorId: message.actorId,
                    eventId: event.id,
                });
            }
        });
    }
    onUpdated(message) {
        return __awaiter(this, void 0, void 0, function* () {
            // TODO
        });
    }
};
TransactionHandler = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Sequelize_1.Sequelize,
        EventBus_1.EventBus])
], TransactionHandler);
exports.TransactionHandler = TransactionHandler;
