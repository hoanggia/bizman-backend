import { injectable as autoInject } from '../../../lib/di';

import { EventBus } from '../../../system/EventBus';
import { Sequelize } from '../../../system/Sequelize';

import { EventCreated } from '../../../app/event/EventCreated';
import { EventUpdated } from '../../../app/event/EventUpdated';


@autoInject
export class TransactionHandler {

	constructor(
		private readonly sequelize: Sequelize,
		bus: EventBus,
	) {
		bus.subscribe(EventCreated, event => this.onCreated(event));
		bus.subscribe(EventUpdated, event => this.onUpdated(event));
	}


	private async onCreated(message: EventCreated) {

		const { models } = this.sequelize;
		const { event } = message;

		if (!event.data.devices) return;
		const devices = event.data.devices.concat(event.data.ownDevices);

		const quantityMap = {};

		for (const device of devices) {

			const { productId } = device;

			if (quantityMap[productId]) {
				quantityMap[productId]++;
			} else {
				quantityMap[productId] = 1;
			}

		}

		const aCase = await event.getCase();
		const { consultantId } = aCase;

		for (const productId in quantityMap) {

			const stock = await models.stock.findOne({
				where: { consultantId, productId },
			});

			let quantity = stock ? stock.quantity : 0;
			quantity = quantity - quantityMap[productId];

			await models.stock.upsert({
				quantity,
				productId,
				consultantId,
			} as any);

			await models.transaction.create({

				date: new Date,
				quantity: -quantityMap[productId],

				consultantId,
				productId: +productId,

				actorId: message.actorId,
				eventId: event.id,

			} as any);

		}

	}


	private async onUpdated(message: EventUpdated) {
		// TODO
	}

}
