"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../../lib/di");
const lib_1 = require("../../../app/lib");
const Sequelize_1 = require("../../../system/Sequelize");
let TransactionStore = class TransactionStore {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }
    findConsultants(productId) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const attributes = ['id', 'firstName', 'lastName'];
            const include = [
                { model: models.stock, where: { productId }, required: false },
            ];
            const where = { role: lib_1.RoleList.Consultant };
            const users = yield models.user.findAll({ attributes, include, where });
            return users.map(transformUser);
        });
    }
    findTransactions(productId, consultantId) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const include = [
                {
                    model: models.event,
                    required: false,
                    attributes: ['id', 'type', 'caseId'],
                    include: [{
                            model: models.case,
                            attributes: ['id', 'patientId'],
                            include: [{ model: models.patient, attributes: ['firstName', 'lastName'] }],
                        }],
                }, {
                    model: models.user,
                    as: 'actor',
                    required: false,
                    attributes: ['id', 'firstName', 'lastName'],
                },
            ];
            const where = { productId, consultantId };
            const list = yield models.transaction.findAll({ include, where });
            return list.map(transformTransaction);
        });
    }
};
TransactionStore = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [Sequelize_1.Sequelize])
], TransactionStore);
exports.TransactionStore = TransactionStore;
function transformUser(user) {
    const { stocks } = user;
    return {
        id: user.id,
        name: user.fullName,
        quantity: stocks[0] ? stocks[0].quantity : 0,
    };
}
function transformTransaction(item) {
    const result = {
        date: item.date,
        quantity: item.quantity,
    };
    const { event, actor } = item;
    if (event) {
        Object.assign(result, {
            event: { id: event.id, type: event.type },
            case: { id: event.case.id },
            patient: { name: event.case.patient.fullName },
        });
    }
    else {
        result.actor = {
            id: actor.id,
            name: actor.fullName,
        };
    }
    return result;
}
