import { injectable as autoInject } from '../../../lib/di';
import { RoleList as Role } from '../../../app/lib';
import { Sequelize } from '../../../system/Sequelize';

import { UserInstance } from '../../../persistence/misc/user';
import { TransactionInstance } from '../../../persistence/product/transaction';


@autoInject
export class TransactionStore {

	constructor(
		private readonly sequelize: Sequelize,
	) {}


	async findConsultants(productId: number) {

		const { models } = this.sequelize;

		const attributes = ['id', 'firstName', 'lastName'];

		const include = [
			{ model: models.stock, where: { productId }, required: false },
		];

		const where = { role: Role.Consultant };

		const users = await models.user.findAll({ attributes, include, where });

		return users.map(transformUser);

	}


	async findTransactions(productId: number, consultantId: number) {

		const { models } = this.sequelize;

		const include = [
			{
				model: models.event,
				required: false,
				attributes: ['id', 'type', 'caseId'],
				include: [{
					model: models.case,
					attributes: ['id', 'patientId'],
					include: [{ model: models.patient, attributes: ['firstName', 'lastName'] }],
				}],
			}, {
				model: models.user,
				as: 'actor',
				required: false,
				attributes: ['id', 'firstName', 'lastName'],
			},
		];

		const where = { productId, consultantId };

		const list = await models.transaction.findAll({ include, where });

		return list.map(transformTransaction);

	}

}


function transformUser(user: UserInstance) {
	const { stocks } = user as any;
	return {
		id: user.id,
		name: user.fullName,
		quantity: stocks[0] ? stocks[0].quantity : 0,
	};
}


function transformTransaction(item: TransactionInstance) {

	const result = {
		date: item.date,
		quantity: item.quantity,
	} as any;

	const { event, actor } = item;

	if (event) {
		Object.assign(result, {
			event: { id: event.id, type: event.type },
			case: { id: event.case.id },
			patient: { name: event.case.patient.fullName },
		});
	} else {
		result.actor = {
			id: actor.id,
			name: actor.fullName,
		};
	}

	return result;

}
