import { injectable as autoInject } from '../../lib/di';

import { TransactionController } from './delivery/TransactionController';


@autoInject
export class TransactionQuerySide {

	constructor(
		controller: TransactionController,
	) {}

}
