"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../lib/di");
const TransactionController_1 = require("./delivery/TransactionController");
let TransactionQuerySide = class TransactionQuerySide {
    constructor(controller) { }
};
TransactionQuerySide = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [TransactionController_1.TransactionController])
], TransactionQuerySide);
exports.TransactionQuerySide = TransactionQuerySide;
