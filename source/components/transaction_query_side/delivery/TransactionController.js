"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("../../../lib/di");
const lib_1 = require("../../../app/lib");
const Router_1 = require("../../../system/Router");
const TransactionStore_1 = require("../storage/TransactionStore");
let TransactionController = class TransactionController {
    constructor(store, router) {
        this.store = store;
        router.register(this);
    }
    findConsultants(context, productId) {
        context.body = this.store.findConsultants(+productId);
    }
    findTransactions(context, productId, consultantId) {
        context.body = this.store.findTransactions(+productId, +consultantId);
    }
};
__decorate([
    lib_1.route.get('/products/:productId/consultants'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", void 0)
], TransactionController.prototype, "findConsultants", null);
__decorate([
    lib_1.route.get('/products/:productId/consultants/:consultantId'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String]),
    __metadata("design:returntype", void 0)
], TransactionController.prototype, "findTransactions", null);
TransactionController = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [TransactionStore_1.TransactionStore,
        Router_1.Router])
], TransactionController);
exports.TransactionController = TransactionController;
