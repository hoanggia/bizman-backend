import { injectable as autoInject } from '../../../lib/di';
import { route as Http, Context } from '../../../app/lib';
import { Router } from '../../../system/Router';

import { TransactionStore } from '../storage/TransactionStore';


@autoInject
export class TransactionController {

	constructor(
		private readonly store: TransactionStore,
		router: Router,
	) {
		router.register(this);
	}


	@Http.get('/products/:productId/consultants')

	private findConsultants(context: Context, productId: string) {
		context.body = this.store.findConsultants(+productId);
	}


	@Http.get('/products/:productId/consultants/:consultantId')

	private findTransactions(context: Context, productId: string, consultantId: string) {
		context.body = this.store.findTransactions(+productId, +consultantId);
	}

}
