"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const StatusList = [
    'sent',
    'accepted',
    'rejected',
    'required',
    'completed',
    'reported',
    'downloaded',
];
function default_1(sequelize) {
    sequelize.define('study', {
        files: {
            type: lib_1.Types.ARRAY(lib_1.Types.STRING),
            defaultValue: [],
        },
        tasks: {
            type: lib_1.Types.JSONB,
        },
        requestViewed: {
            type: lib_1.Types.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        studyDate: {
            type: lib_1.Types.INTEGER,
        },
        approvedAt: {
            type: lib_1.Types.INTEGER,
        },
        completedAt: {
            type: lib_1.Types.INTEGER,
        },
        reportedAt: {
            type: lib_1.Types.INTEGER,
        },
        ahi: {
            type: lib_1.Types.FLOAT,
        },
        cpapRecommended: {
            type: lib_1.Types.BOOLEAN,
        },
        status: {
            type: lib_1.Types.ENUM(StatusList),
            allowNull: false,
            defaultValue: StatusList[0],
        },
        money: {
            type: lib_1.Types.JSONB,
        },
        readers: {
            type: lib_1.Types.JSONB,
        },
        studyAppointment: {
            type: lib_1.Types.JSONB,
        },
        completeDocumentFile: {
            type: lib_1.Types.ARRAY(lib_1.Types.STRING),
            defaultValue: [],
        },
    }, {
        timestamps: false,
    });
}
exports.default = default_1;
