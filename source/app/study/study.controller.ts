import * as fs from 'fs';

import { resource, route } from '../lib';
import { Context, ResourceController } from '../lib';

import { FileController } from '../file/file.controller';

import { StudyService } from './study.service';


@resource('studies')
export class StudyController extends ResourceController {

	constructor(
		private fileCtrl: FileController,
		service: StudyService
	) {
		super(service);
	}


	@route.post('/studies/:id/download')

	async download(context: Context, id: string) {
		const fileId = await (this.service as StudyService).download(+id);
		await this.fileCtrl.view(context, fileId);
	}


	@route.post('/studies/:id/read')

	async read(context: Context, id: string) {
		(this.service as StudyService).read(+id);
	}

}
