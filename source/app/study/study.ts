import { Models, Sequelize, Types } from '../lib';


const StatusList = [
	'sent',
	'accepted' ,
	'rejected' ,
	'required' ,
	'completed',
	'reported' ,
	'downloaded',
];


export default function(sequelize: Sequelize) {

	sequelize.define('study', {

		files: {
			type: Types.ARRAY(Types.STRING),
			defaultValue: [],
		},

		tasks: {
			type: Types.JSONB,
		},

		requestViewed: {
			type: Types.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

		studyDate: {
			type: Types.INTEGER,
		},

		approvedAt: {
			type: Types.INTEGER,
		},

		completedAt: {
			type: Types.INTEGER,
		},

		reportedAt: {
			type: Types.INTEGER,
		},

		ahi: {
			type: Types.FLOAT,
		},

		cpapRecommended: {
			type: Types.BOOLEAN,
		},

		status: {
			type: Types.ENUM(StatusList),
			allowNull: false,
			defaultValue: StatusList[0],
		},

		money: {
			type: Types.JSONB,
		},

		readers: {
			type: Types.JSONB,
		},
		studyAppointment: {
			type: Types.JSONB,
		},
		completeDocumentFile: {
			type: Types.ARRAY(Types.STRING),
			defaultValue: [],
		},

	}, {

		timestamps: false,

	});

}
