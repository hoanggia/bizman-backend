import * as path from 'path';

import * as lodash from 'lodash';
import * as moment from 'moment';

import { injectable, fillForm, ResourceService } from '../lib';
import { Config, Mailer, Sequelize, State } from '../lib';

import { SettingService } from '../setting/setting.service';


@injectable
export class StudyService extends ResourceService {

	private assignmentPdf: string;
	private feedbackPdf: string;


	constructor(
		private mailer: Mailer,
		private setting: SettingService,
		private sequelize: Sequelize,
		private state: State,
		config: Config
	) {
		super(sequelize.models.study);

		mailer.register('study/report', null);
		mailer.register('study/request', 'You have a new case');
		// mailer.register('study/notification', 'You have a New Sleep Study Documents');

		this.assignmentPdf = path.join(config.baseDir, 'study/db4a-1405.pdf');
		this.feedbackPdf = path.join(config.baseDir, 'study/feedback.pdf');
	}


	async create(data) {

		const { models } = this.sequelize;

		data.tasks = this.setting.get('actions').map(item => ({
			done: false,
			meta: item.meta,
			work: item.name,
		}));

		const study = await super.create(data);

		const event = await models.event.findById(data.eventId);
		const specialist = await models.user.findById(event.reportingSpecialistId);

		if (specialist) {
			this.mailer.send('study/request', specialist.email, { event, specialist });
		}

		return study;

	}


	async update(id: number, data) {

		const { models } = this.sequelize;

		const study = await models.study.findById(id);

		const event = await models.event.findById(data.eventId);
		const kase = await models.case.findById(event.caseId);
		const patient = kase['patient'];
		const consultant = await models.user.findById(kase.consultantId);


		// Neu co file upload cua Completed Sleep Study Documents thi send mail

		this.mailer.register('study/notification', consultant.fullName.toUpperCase() + ' New Sleep Study Documents for Patient ' + patient.fullName);

		if(data.completeDocumentFile.length > 0 && data.completeDocumentFile[0] != study.completeDocumentFile[0])
		{
			this.mailer.send('study/notification', 'study@clmsleep.com', {event, kase, patient});
		}

		// Neu tasks Completed thi send mail

		this.mailer.register('study/completed', consultant.fullName.toUpperCase() + ' New Claim for Patient ' + patient.fullName);


		if (study.files[0] === data.files[0]) {

			await study.update(data);

			if( data.status === 'completed' && data.status === study.status)
			{
				this.mailer.send('study/completed', 'study@clmsleep.com', {event, kase, patient});
			}

			// const event = await models.event.findById(data.eventId);

			return;

		}

		// const event = await models.event.findById(data.eventId);
		// const kase = await models.case.findById(event.caseId);

		// const patient = kase['patient'];

		const people = [
			event.referringDoctorId,
			event.dentistId,
		];


		const list = people.filter(id => data.selected.includes(id));

		data.readers = list.map(id => ({
			userId: id,
		}));

		data.status = 'reported';

		await study.update(data);

		for (const id of list) {

			const user = await models.user.findById(id);

			const locals = {
				kase,
				user,
				event,
				patient,
			};

			const subject = 'New report for ' + patient.fullName;

			this.mailer.send('study/report', user.email, locals, subject);

		}


	}


	async download(id: number): Promise<string> {
		const { models } = this.sequelize;
		const study = await models.study.findById(id);
		study.status = 'downloaded';
		study.save();
		return study.files[0];
	}


	async read(id: number) {

		const { models } = this.sequelize;
		const { payload } = this.state;

		const study = await models.study.findById(id);

		for (const item of study.readers) {
			if (item.userId === payload.id) {
				item.readAt = moment().unix();
				break;
			}
		}

		await study.save();

	}


	async fillAssignment(id: number): Promise<string> {

		const { models } = this.sequelize;

		const study = await models.study.findById(id, {
			attributes: ['id', 'eventId', 'studyDate'],
		});

		const include = [
			{ model: models.case, include: [models.patient] },
			{ model: models.user, as: 'referringDoctor' },
			{ model: models.user, as: 'reportingSpecialist', attributes: ['firstName', 'lastName'] },
		];

		const event = await models.event.findById(study.eventId, { include });

		const { referringDoctor, reportingSpecialist } = event as any;
		const { patient } = (event as any).case;

		const { providerNo } = referringDoctor.data;
		const { medicareNo } = patient;

		const isoRefDate = moment.unix(event.date).format('DDMMYY');
		const dateOfBirth = moment.unix(patient.dateOfBirth);

		// LS1 - LS6: LSPN
		// EQ1 - EQ6: Equipment Number
		// Des1 - Des5: Description of Service

		const RefPrac = referringDoctor.fullName + '\n' + (referringDoctor.address || '');

		const fields = {
			Firstname: patient.firstName,
			Surname: patient.lastName,
			Add1: patient.address.line,
			DOB: dateOfBirth.format('DD/MM/YYYY'),
			'2.DD1': isoRefDate[0],
			'2.DD2': isoRefDate[1],
			'2.MM1': isoRefDate[2],
			'2.MM2': isoRefDate[3],
			'2.YY1': isoRefDate[4],
			'2.YY2': isoRefDate[5],
			RefPrac,
			Ref: medicareNo[medicareNo.length - 1],
			Provider: reportingSpecialist.fullName,
		};

		if (medicareNo) {
			for (let i = 0; i < medicareNo.length; i++) {
				fields['MA' + (i + 1)] = medicareNo[i];
			}
		}

		if (providerNo) {
			for (let i = 0; i < providerNo.length; i++) {
				fields['PN' + (i + 1)] = providerNo[i];
			}
		}

		if (study.studyDate) {
			const isoStudyDate = moment.unix(study.studyDate).format('DDMMYY');
			Object.assign(fields, {
				DD1: isoStudyDate[0],
				DD2: isoStudyDate[1],
				MM1: isoStudyDate[2],
				MM2: isoStudyDate[3],
				YY1: isoStudyDate[4],
				YY2: isoStudyDate[5],
			});
		}

		const destPath = getPdfPath(study.id, 'assignment');
		await fillForm(this.assignmentPdf, destPath, fields);
		return destPath;

	}


	async fillFeedback(id: number): Promise<string> {

		const { models } = this.sequelize;

		const study = await models.study.findById(id, {
			attributes: ['id', 'eventId', 'studyDate'],
		});

		const attributes = ['id', 'caseId'];

		const include = [
			{
				model: models.case,
				attributes: ['id', 'patientId'],
				include: [
					{
						model: models.patient,
						attributes: ['id', 'firstName', 'lastName'],
					}
				],
			},
			{
				model: models.user,
				as: 'reportingSpecialist',
				attributes: ['firstName', 'lastName'],
			},
		];

		const event = await models.event.findById(study.eventId, { attributes, include });

		const { patient } = (event as any).case;
		const { reportingSpecialist } = event as any;

		const fields = {
			'Study Date': moment.unix(study.studyDate).format('DD/MM/YYYY'),
			'Patient Name': patient.fullName,
			'Reporting Sleep Physician': reportingSpecialist.fullName,
		};

		const destPath = getPdfPath(study.id, 'feedback');
		await fillForm(this.feedbackPdf, destPath, fields);
		return destPath;

	}

}


function getPdfPath(id: number, name: string) {
	const dataDir = process.env.DATA_DIR;
	return path.join(dataDir, 'pdf', `${ name }-${ id }.pdf`);
}
