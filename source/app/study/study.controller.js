"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const file_controller_1 = require("../file/file.controller");
const study_service_1 = require("./study.service");
let StudyController = class StudyController extends lib_2.ResourceController {
    constructor(fileCtrl, service) {
        super(service);
        this.fileCtrl = fileCtrl;
    }
    download(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const fileId = yield this.service.download(+id);
            yield this.fileCtrl.view(context, fileId);
        });
    }
    read(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            this.service.read(+id);
        });
    }
};
__decorate([
    lib_1.route.post('/studies/:id/download'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], StudyController.prototype, "download", null);
__decorate([
    lib_1.route.post('/studies/:id/read'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], StudyController.prototype, "read", null);
StudyController = __decorate([
    lib_1.resource('studies'),
    __metadata("design:paramtypes", [file_controller_1.FileController,
        study_service_1.StudyService])
], StudyController);
exports.StudyController = StudyController;
