"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('case', {
        active: {
            type: lib_1.Types.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        reason: {
            type: lib_1.Types.STRING,
        },
        archiveAt: {
            type: lib_1.Types.INTEGER,
        },
        resumeAt: {
            type: lib_1.Types.INTEGER,
        },
    }, {
        timestamps: false,
        classMethods: { associate },
    });
}
exports.default = default_1;
function associate(models) {
    const kase = models.case;
    kase.belongsTo(models.patient, {
        foreignKey: { allowNull: false },
        onDelete: 'cascade',
    });
    kase.belongsTo(models.user, {
        as: 'consultant',
        onDelete: 'restrict',
    });
    kase.hasMany(models.event, {
        foreignKey: { allowNull: false },
    });
    kase.addScope('defaultScope', {
        include: [models.patient],
    }, {
        override: true,
    });
}
