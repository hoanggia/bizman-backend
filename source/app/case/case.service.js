"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const lib_3 = require("../lib");
let CaseService = class CaseService extends lib_2.ResourceService {
    constructor(sequelize, state) {
        super(sequelize.models.case);
        this.sequelize = sequelize;
        this.state = state;
    }
    index() {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const include = [
                {
                    model: models.patient
                }, {
                    model: models.event,
                    order: [['date', 'DESC']],
                    limit: 1,
                },
            ];
            const where = {};
            if (payload.role === lib_2.RoleList.Consultant) {
                where.consultantId = payload.id;
            }
            return models.case.findAll({ include, where });
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            if (payload.role === lib_2.RoleList.Consultant) {
                data.consultantId = payload.id;
            }
            return models.case.create(data);
        });
    }
    show(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const include = [
                {
                    model: models.patient,
                }, {
                    model: models.event,
                    where: { isDeleted: false },
                    include: [models.note, models.order, models.study, models.user],
                    required: false,
                },
            ];
            const kase = yield models.case.findById(id, { include });
            if (!kase)
                throw new lib_2.NotFound(id);
            if (payload.role === lib_2.RoleList.Consultant) {
                if (kase.consultantId !== payload.id)
                    throw new lib_2.NotFound(id);
            }
            return kase;
        });
    }
    terminate(id, data) {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const user = yield models.user.findById(payload.id, {
                attributes: { include: 'hash' },
            });
            const match = yield lib_1.verify(user.hash, data.password);
            if (!match)
                throw 418;
            return _super("remove").call(this, id);
        });
    }
};
CaseService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_3.Sequelize,
        lib_3.State])
], CaseService);
exports.CaseService = CaseService;
