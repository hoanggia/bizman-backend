"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const co_body_1 = require("co-body");
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const case_service_1 = require("./case.service");
const roleMap = {
    index: [lib_2.RoleList.Admin, lib_2.RoleList.Consultant, lib_2.RoleList.SleepScientist],
    show: [lib_2.RoleList.Admin, lib_2.RoleList.Consultant, lib_2.RoleList.SleepScientist],
    create: [lib_2.RoleList.Admin, lib_2.RoleList.Consultant, lib_2.RoleList.ReferringDoctor],
    update: [lib_2.RoleList.Admin, lib_2.RoleList.SleepScientist],
    remove: [lib_2.RoleList.Admin],
};
let CaseController = class CaseController extends lib_2.ResourceController {
    constructor(service) {
        super(service);
    }
    getRoles(action) {
        return roleMap[action];
    }
    terminate(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield co_body_1.json(context);
            yield this.service.terminate(+id, data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.authorize(),
    lib_1.route.post('/cases/:id/terminate'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], CaseController.prototype, "terminate", null);
CaseController = __decorate([
    lib_1.resource('cases'),
    __metadata("design:paramtypes", [case_service_1.CaseService])
], CaseController);
exports.CaseController = CaseController;
