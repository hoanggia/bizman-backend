import { Models, Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('case', {

		active: {
			type: Types.BOOLEAN,
			allowNull: false,
			defaultValue: true,
		},

		reason: {
			type: Types.STRING,
		},

		archiveAt: {
			type: Types.INTEGER,
		},

		resumeAt: {
			type: Types.INTEGER,
		},

	}, {

		timestamps: false,

		classMethods: { associate },

	});

}


function associate(models: Models) {

	const kase = models.case;

	kase.belongsTo(models.patient, {
		foreignKey: { allowNull: false },
		onDelete: 'cascade',
	});

	kase.belongsTo(models.user, {
		as: 'consultant',
		onDelete: 'restrict',
	});

	kase.hasMany(models.event, {
		foreignKey: { allowNull: false },
	});

	(kase as any).addScope('defaultScope', {
		include: [models.patient],
	}, {
		override: true,
	});

}
