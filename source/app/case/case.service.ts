import { injectable, verify } from '../lib';
import { ResourceService, NotFound, RoleList } from '../lib';
import { Sequelize, State } from '../lib';


@injectable
export class CaseService extends ResourceService {

	constructor(
		private sequelize: Sequelize,
		private state: State
	) {
		super(sequelize.models.case);
	}


	async index() {
		const { models } = this.sequelize;
		const { payload } = this.state;

		const include = [
			{
				model: models.patient
			}, {
				model: models.event,
				order: [['date', 'DESC']],
				limit: 1,
			},
		];

		const where = <any>{};
		if (payload.role === RoleList.Consultant) {
			where.consultantId = payload.id;
		}

		return models.case.findAll({ include, where });
	}


	async create(data) {

		const { models } = this.sequelize;
		const { payload } = this.state;

		if (payload.role === RoleList.Consultant) {
			data.consultantId = payload.id;
		}

		return models.case.create(data);

	}


	async show(id: number) {
		const { models } = this.sequelize;
		const { payload } = this.state;

		const include = [
			{
				model: models.patient,

			}, {
				model: models.event,
				where: {isDeleted:false},
				include: [models.note, models.order, models.study,models.user],
				required:false,
			},
		];

		const kase = await models.case.findById(id, { include });

		if (!kase) throw new NotFound(id);
		if (payload.role === RoleList.Consultant) {
			if (kase.consultantId !== payload.id) throw new NotFound(id);
		}

		return kase;
	}


	async terminate(id: number, data) {
		const { models } = this.sequelize;
		const { payload } = this.state;

		const user = await models.user.findById(payload.id, {
			attributes: <any>{ include: 'hash' },
		});

		const match = await verify(user.hash, data.password);
		if (!match) throw 418;

		return super.remove(id);
	}

}
