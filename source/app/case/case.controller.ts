import { json as parse } from 'co-body';

import { authorize, resource, route } from '../lib';
import { Context, ResourceController, RoleList } from '../lib';

import { CaseService } from './case.service';


const roleMap = {
	index : [RoleList.Admin, RoleList.Consultant, RoleList.SleepScientist],
	show  : [RoleList.Admin, RoleList.Consultant, RoleList.SleepScientist],

	create: [RoleList.Admin, RoleList.Consultant, RoleList.ReferringDoctor],
	update: [RoleList.Admin, RoleList.SleepScientist],
	remove: [RoleList.Admin],
};


@resource('cases')
export class CaseController extends ResourceController {

	constructor(service: CaseService) {
		super(service);
	}


	protected getRoles(action: string) {
		return roleMap[action];
	}


	@authorize()
	@route.post('/cases/:id/terminate')
	async terminate(context: Context, id: string) {
		const data = await parse(context);
		await (this.service as CaseService).terminate(+id, data);
		context.status = 204; // No Content
	}

}
