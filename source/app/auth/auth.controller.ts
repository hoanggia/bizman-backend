import * as lodash from 'lodash';

import { injectable, Jwt, State } from '../lib';
import { generate, parse, route } from '../lib';
import { BadRequest, Context, Unauthorized } from '../lib';

import { AuthService } from './auth.service';


const JWT = 'jwt';
const XSRF = 'XSRF-TOKEN';


@injectable
export class AuthController {

	constructor(
		private jwt: Jwt,
		private service: AuthService,
		private state: State
	) {}


	private isPublic(context: Context) {
		if (context.method === 'POST' && context.path === '/auth') return true;
		if (context.method === 'GET' && /^\/files\/[0-9a-f-]+\/view$/.test(context.path)) return true;
		return false;
	}


	private async handler(context: Context, next) {

		if (!this.isPublic(context)) {
			const jwt = context.cookies.get(JWT);
			const xsrf = context.get(XSRF);

			const payload = await this.verify(jwt, xsrf);
			this.state.payload = payload;
		}

		await next();

	}


	get auth() {
		return this.handler.bind(this);
	}


	@route.post('/auth')
	async logIn(context: Context) {

		const credential = await parse(context);
		const user = await this.service.logIn(credential);
		const { jwt, xsrf } = this.sign(user);

		context.cookies.set(JWT, jwt);
		context.cookies.set(XSRF, xsrf, { httpOnly: false });
		context.body = lodash.pick(user, 'id', 'firstName', 'role');

	}


	@route.delete('/auth')
	async logOut(context: Context) {
		context.cookies.set(JWT);
	}


	private async verify(jwt: string, xsrf: string) {

		try {
			const payload = await this.jwt.verify(jwt);
			if (xsrf !== payload.xsrf) throw new BadRequest('XSRF');
			return payload;
		} catch (error) {
			throw new Unauthorized();
		}

	}


	private sign(user) {

		const xsrf = generate();

		const payload = {
			id  : user.id,
			role: user.role,
			xsrf,
		};

		const jwt = this.jwt.sign(payload);

		return { jwt, xsrf };

	}

}
