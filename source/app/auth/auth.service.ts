import { injectable, Sequelize } from '../lib';
import { verify, Unauthorized } from '../lib';


@injectable
export class AuthService {

	constructor(private sequelize: Sequelize) {}


	async logIn(credential) {
		const { models } = this.sequelize;

		const user = await models.user.find({
			attributes: <any>{ include: 'hash' },
			where: { email: credential.email },
		});
		if (!user) throw new Unauthorized();

		const match = await verify(user.hash, credential.password);
		if (!match) throw new Unauthorized();

		return user;
	}

}
