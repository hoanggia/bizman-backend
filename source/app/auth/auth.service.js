"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
let AuthService = class AuthService {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }
    logIn(credential) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const user = yield models.user.find({
                attributes: { include: 'hash' },
                where: { email: credential.email },
            });
            if (!user)
                throw new lib_2.Unauthorized();
            const match = yield lib_2.verify(user.hash, credential.password);
            if (!match)
                throw new lib_2.Unauthorized();
            return user;
        });
    }
};
AuthService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize])
], AuthService);
exports.AuthService = AuthService;
