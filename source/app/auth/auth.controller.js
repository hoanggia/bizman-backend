"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const lib_3 = require("../lib");
const auth_service_1 = require("./auth.service");
const JWT = 'jwt';
const XSRF = 'XSRF-TOKEN';
let AuthController = class AuthController {
    constructor(jwt, service, state) {
        this.jwt = jwt;
        this.service = service;
        this.state = state;
    }
    isPublic(context) {
        if (context.method === 'POST' && context.path === '/auth')
            return true;
        if (context.method === 'GET' && /^\/files\/[0-9a-f-]+\/view$/.test(context.path))
            return true;
        return false;
    }
    handler(context, next) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isPublic(context)) {
                const jwt = context.cookies.get(JWT);
                const xsrf = context.get(XSRF);
                const payload = yield this.verify(jwt, xsrf);
                this.state.payload = payload;
            }
            yield next();
        });
    }
    get auth() {
        return this.handler.bind(this);
    }
    logIn(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const credential = yield lib_2.parse(context);
            const user = yield this.service.logIn(credential);
            const { jwt, xsrf } = this.sign(user);
            context.cookies.set(JWT, jwt);
            context.cookies.set(XSRF, xsrf, { httpOnly: false });
            context.body = lodash.pick(user, 'id', 'firstName', 'role');
        });
    }
    logOut(context) {
        return __awaiter(this, void 0, void 0, function* () {
            context.cookies.set(JWT);
        });
    }
    verify(jwt, xsrf) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const payload = yield this.jwt.verify(jwt);
                if (xsrf !== payload.xsrf)
                    throw new lib_3.BadRequest('XSRF');
                return payload;
            }
            catch (error) {
                throw new lib_3.Unauthorized();
            }
        });
    }
    sign(user) {
        const xsrf = lib_2.generate();
        const payload = {
            id: user.id,
            role: user.role,
            xsrf,
        };
        const jwt = this.jwt.sign(payload);
        return { jwt, xsrf };
    }
};
__decorate([
    lib_2.route.post('/auth'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logIn", null);
__decorate([
    lib_2.route.delete('/auth'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logOut", null);
AuthController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Jwt,
        auth_service_1.AuthService,
        lib_1.State])
], AuthController);
exports.AuthController = AuthController;
