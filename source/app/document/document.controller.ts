import { resource, ResourceController, RoleList } from '../lib';

import { DocumentService } from './document.service';


const roleMap = {
	create: RoleList.Admin,
	update: RoleList.Admin,
	remove: RoleList.Admin,
};


@resource('documents')
export class DocumentController extends ResourceController {

	constructor(service: DocumentService) {
		super(service);
	}


	protected getRoles(action: string) {
		return roleMap[action];
	}

}
