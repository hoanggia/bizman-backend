"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const document_service_1 = require("./document.service");
const roleMap = {
    create: lib_1.RoleList.Admin,
    update: lib_1.RoleList.Admin,
    remove: lib_1.RoleList.Admin,
};
let DocumentController = class DocumentController extends lib_1.ResourceController {
    constructor(service) {
        super(service);
    }
    getRoles(action) {
        return roleMap[action];
    }
};
DocumentController = __decorate([
    lib_1.resource('documents'),
    __metadata("design:paramtypes", [document_service_1.DocumentService])
], DocumentController);
exports.DocumentController = DocumentController;
