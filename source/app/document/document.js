"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('document', {
        name: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
    }, {
        timestamps: false,
        classMethods: { associate },
    });
}
exports.default = default_1;
function associate(models) {
    const { document } = models;
    document.belongsTo(models.file, {
        foreignKey: { allowNull: false },
    });
}
