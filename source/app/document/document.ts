import { Models, Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('document', {

		name: {
			type: Types.STRING,
			allowNull: false,
		},

	}, {

		timestamps: false,

		classMethods: { associate },

	});

}


function associate(models: Models) {

	const { document } = models;

	document.belongsTo(models.file, {
		foreignKey: { allowNull: false },
	});

}
