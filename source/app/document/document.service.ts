import { injectable, ResourceService, Sequelize } from '../lib';


@injectable
export class DocumentService extends ResourceService {

	constructor(sequelize: Sequelize) {
		super(sequelize.models.document);
	}

}
