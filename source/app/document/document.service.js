"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
let DocumentService = class DocumentService extends lib_1.ResourceService {
    constructor(sequelize) {
        super(sequelize.models.document);
    }
};
DocumentService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize])
], DocumentService);
exports.DocumentService = DocumentService;
