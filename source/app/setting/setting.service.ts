import * as fs from 'fs';
import * as path from 'path';

import { injectable } from '../lib';


@injectable
export class SettingService {

	private filePath: string;
	private settings: any;


	constructor() {
		this.filePath = path.join(process.env.DATA_DIR, 'settings.json');

		const json = fs.readFileSync(this.filePath, 'utf8');
		this.settings = JSON.parse(json);
	}


	get(name: string){
		return this.settings[name] || [];
	}


	put(name: string, data: any) {
		this.settings[name] = data;

		const json = JSON.stringify(this.settings, null, '\t');
		fs.writeFileSync(this.filePath, json);
	}

}
