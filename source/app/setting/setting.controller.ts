import { json as parse } from 'co-body';

import { authorize, injectable, route } from '../lib';
import { Context } from '../lib';

import { SettingService } from './setting.service';


@injectable
export class SettingController {

	constructor(private service: SettingService) {}


	@authorize()
	@route.get('/settings/:name')
	async get(context: Context, name) {
		context.body = this.service.get(name);
	}


	@authorize()
	@route.put('/settings/:name')
	async put(context: Context, name) {
		const data = await parse(context);
		this.service.put(name, data);
		context.status = 204; // No Content
	}

}
