"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const co_body_1 = require("co-body");
const lib_1 = require("../lib");
const setting_service_1 = require("./setting.service");
let SettingController = class SettingController {
    constructor(service) {
        this.service = service;
    }
    get(context, name) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = this.service.get(name);
        });
    }
    put(context, name) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield co_body_1.json(context);
            this.service.put(name, data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.authorize(),
    lib_1.route.get('/settings/:name'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "get", null);
__decorate([
    lib_1.authorize(),
    lib_1.route.put('/settings/:name'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "put", null);
SettingController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [setting_service_1.SettingService])
], SettingController);
exports.SettingController = SettingController;
