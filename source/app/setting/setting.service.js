"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const lib_1 = require("../lib");
let SettingService = class SettingService {
    constructor() {
        this.filePath = path.join(process.env.DATA_DIR, 'settings.json');
        const json = fs.readFileSync(this.filePath, 'utf8');
        this.settings = JSON.parse(json);
    }
    get(name) {
        return this.settings[name] || [];
    }
    put(name, data) {
        this.settings[name] = data;
        const json = JSON.stringify(this.settings, null, '\t');
        fs.writeFileSync(this.filePath, json);
    }
};
SettingService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [])
], SettingService);
exports.SettingService = SettingService;
