import { injectable, ResourceService } from '../lib';
import { Mailer, Sequelize, State } from '../lib';
import { generate, hash, RoleList } from '../lib';


@injectable
export class UserService extends ResourceService {

	constructor(
		private mailer: Mailer,
		private sequelize: Sequelize,
		private state: State
	) {
		super(sequelize.models.user);
		mailer.register('user/new', 'Welcome to CLM BizMan');
		mailer.register('user/reset', 'CLM Bizman - Your Password Has Been Reset ');
	}


	async index() {
		const { models } = this.sequelize;
		const { payload } = this.state;

		const attributes = ['id', 'firstName', 'lastName', 'role', 'data'];

		if (payload.role === RoleList.Admin) attributes.push('email');

		return models.user.findAll({ attributes, raw: true, order: '"lastName" ASC'  });
	}


	async create(data) {
		const { models } = this.sequelize;

		const password = generate();
		data.hash = await hash(password);

		if (!data.data) data.data = {};
		const user = await models.user.create(data);

		const locals = { user, password };
		this.mailer.send('user/new', user.email, locals);

		return user;
	}

	async reset(id: number, data){

		const { models } = this.sequelize;

		const password = generate();
		data.hash = await hash(password);
		const user = await models.user.findById(data.id, {
			attributes: <any>{ include: 'hash' },
		});

		await user.update(data);
		const locals = { user, password };
		this.mailer.send('user/reset', user.email, locals);
	}

}
