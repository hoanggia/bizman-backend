"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
function default_1(sequelize) {
    const values = Object.keys(lib_2.RoleList).map(key => lib_2.RoleList[key]);
    sequelize.define('user', {
        firstName: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
        lastName: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
        role: {
            type: lib_1.Types.ENUM(values),
            allowNull: false,
        },
        email: {
            type: lib_1.Types.STRING,
            unique: true,
        },
        hash: {
            type: lib_1.Types.STRING,
        },
        address: {
            type: lib_1.Types.STRING,
        },
        address2: {
            type: lib_1.Types.JSONB,
        },
        phones: {
            type: lib_1.Types.JSONB,
        },
        data: {
            type: lib_1.Types.JSONB,
        },
    }, {
        timestamps: false,
        classMethods: { associate },
        getterMethods: { fullName },
    });
}
exports.default = default_1;
function associate(models) {
    models.user.addScope('defaultScope', {
        attributes: { exclude: 'hash' },
    }, {
        override: true,
    });
}
function fullName() {
    return this.firstName + ' ' + this.lastName;
}
