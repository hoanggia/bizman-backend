"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const user_service_1 = require("./user.service");
const roleMap = {
    create: lib_1.RoleList.Admin,
    update: lib_1.RoleList.Admin,
    remove: lib_1.RoleList.Admin,
    reset: lib_1.RoleList.Admin,
};
let UserController = class UserController extends lib_1.ResourceController {
    constructor(service) {
        super(service);
    }
    getRoles(action) {
        return roleMap[action];
    }
    reset(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield lib_1.parse(context);
            yield this.service.reset(+id, data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.route.post('/users/:id/reset'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "reset", null);
UserController = __decorate([
    lib_1.resource('users'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
