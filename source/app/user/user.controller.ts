import { resource, ResourceController, RoleList, route, Context, parse } from '../lib';

import { UserService } from './user.service';


const roleMap = {
	create: RoleList.Admin,
	update: RoleList.Admin,
	remove: RoleList.Admin,
	reset: RoleList.Admin,
};


@resource('users')
export class UserController extends ResourceController {

	constructor(service: UserService) {
		super(service);
	}


	protected getRoles(action: string) {
		return roleMap[action];
	}

	@route.post('/users/:id/reset')
	async reset(context: Context,id: string) {
		const data = await parse(context);
		await (this.service as UserService).reset(+id, data);
		context.status = 204; // No Content
	}

}
