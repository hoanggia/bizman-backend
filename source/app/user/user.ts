import { Models, Sequelize, Types } from '../lib';
import { RoleList } from '../lib';


export default function(sequelize: Sequelize) {

	const values = Object.keys(RoleList).map(key => RoleList[key]);

	sequelize.define('user', {

		firstName: {
			type: Types.STRING,
			allowNull: false,
		},

		lastName: {
			type: Types.STRING,
			allowNull: false,
		},

		role: {
			type: Types.ENUM(values),
			allowNull: false,
		},

		email: {
			type: Types.STRING,
			unique: true,
		},

		hash: {
			type: Types.STRING,
		},

		address: {
			type: Types.STRING,
		},
		address2:{
			type: Types.JSONB,
		},
		phones: {
			type: Types.JSONB,
		},

		data: {
			type: Types.JSONB,
		},

	}, {

		timestamps: false,

		classMethods: { associate },

		getterMethods: { fullName },

	});

}


function associate(models: Models) {

	(models.user as any).addScope('defaultScope', {
		attributes: { exclude: 'hash' },
	}, {
		override: true,
	});

}


function fullName() {
	return this.firstName + ' ' + this.lastName;
}
