"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const lib_3 = require("../lib");
let UserService = class UserService extends lib_1.ResourceService {
    constructor(mailer, sequelize, state) {
        super(sequelize.models.user);
        this.mailer = mailer;
        this.sequelize = sequelize;
        this.state = state;
        mailer.register('user/new', 'Welcome to CLM BizMan');
        mailer.register('user/reset', 'CLM Bizman - Your Password Has Been Reset ');
    }
    index() {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const attributes = ['id', 'firstName', 'lastName', 'role', 'data'];
            if (payload.role === lib_3.RoleList.Admin)
                attributes.push('email');
            return models.user.findAll({ attributes, raw: true, order: '"lastName" ASC' });
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const password = lib_3.generate();
            data.hash = yield lib_3.hash(password);
            if (!data.data)
                data.data = {};
            const user = yield models.user.create(data);
            const locals = { user, password };
            this.mailer.send('user/new', user.email, locals);
            return user;
        });
    }
    reset(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const password = lib_3.generate();
            data.hash = yield lib_3.hash(password);
            const user = yield models.user.findById(data.id, {
                attributes: { include: 'hash' },
            });
            yield user.update(data);
            const locals = { user, password };
            this.mailer.send('user/reset', user.email, locals);
        });
    }
};
UserService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_2.Mailer,
        lib_2.Sequelize,
        lib_2.State])
], UserService);
exports.UserService = UserService;
