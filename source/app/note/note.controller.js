"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const note_service_1 = require("./note.service");
let NoteController = class NoteController extends lib_2.ResourceController {
    constructor(service) {
        super(service);
    }
    send(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const selected = yield lib_1.parse(context);
            yield this.service.send(+id, selected);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.route.post('/notes/:id/send'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], NoteController.prototype, "send", null);
NoteController = __decorate([
    lib_1.resource('notes'),
    __metadata("design:paramtypes", [note_service_1.NoteService])
], NoteController);
exports.NoteController = NoteController;
