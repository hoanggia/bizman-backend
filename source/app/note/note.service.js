"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const moment = require("moment");
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const lib_3 = require("../lib");
let NoteService = class NoteService extends lib_1.ResourceService {
    constructor(mailer, sequelize) {
        super(sequelize.models.note);
        this.mailer = mailer;
        this.sequelize = sequelize;
        mailer.register('note/note', null);
    }
    send(id, selected) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const def = {
                model: models.event,
                attributes: ['id'],
                include: [
                    { model: models.case, attributes: ['id'] },
                    { model: models.study, attributes: ['id'] },
                    { model: models.order, attributes: ['type'] },
                    { model: models.user, attributes: ['id', 'email'], as: 'referringDoctor' },
                    { model: models.user, attributes: ['id', 'email'], as: 'dentist' },
                ],
            };
            const note = yield models.note.findById(id, {
                include: [def]
            });
            const { event } = note;
            const kase = yield models.case.findById(event.case.id, {
                include: [
                    { model: models.patient, attributes: ['firstName', 'lastName'] },
                ],
            });
            const { patient } = kase;
            const users = [
                event.referringDoctor,
                event.dentist,
            ];
            const list = users
                .filter(user => user)
                .filter(user => selected.includes(user.id))
                .map(user => user.email);
            const recipients = lodash.uniq(list);
            const locals = {
                kase: {
                    id: kase.id,
                    name: patient.fullName,
                },
                event: {
                    id: event.id,
                    name: getEventName(event),
                },
                note: {
                    name: getNoteName(note),
                    date: moment.unix(note.date).format('L'),
                },
            };
            const subject = `${locals.kase.name} » ${locals.event.name} » ` +
                `${locals.note.name} at ${locals.note.date}`;
            this.mailer.send('note/note', recipients, locals, subject);
        });
    }
};
NoteService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_2.Mailer,
        lib_2.Sequelize])
], NoteService);
exports.NoteService = NoteService;
function getEventName(event) {
    if (event.study)
        return 'BizMan (Sleep Study)';
    return lib_3.TypeLabel[event.order.type];
}
function getNoteName(note) {
    return lib_3.NoteTypeLabel[note.type];
}
