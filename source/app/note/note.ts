import { Models, Sequelize, Types } from '../lib';
import { NoteTypeList } from '../lib';


export default function(sequelize: Sequelize) {

	const values = Object.keys(NoteTypeList).map(key => NoteTypeList[key]);

	sequelize.define('note', {

		files: {
			type: Types.ARRAY(Types.STRING),
			defaultValue: [],
		},

		type: {
			type: Types.ENUM(values),
			allowNull: false,
		},

		date: {
			type: Types.INTEGER,
			allowNull: false,
		},

		data: {
			type: Types.JSONB,
			allowNull: false,
		},

	}, {

		timestamps: false,

		classMethods: { associate },

	});

}


function associate(models: Models) {

	const { note } = models;

	note.belongsTo(models.event, {
		foreignKey: { allowNull: false },
		onDelete: 'cascade',
	});

	note.belongsTo(models.user,{
		as: 'createdBy',
		onDelete: 'restrict',
	});

	note.belongsTo(models.user,{
		as: 'updatedBy',
		onDelete: 'restrict',
	});

	note.belongsTo(models.user);

}
