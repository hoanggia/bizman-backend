import * as lodash from 'lodash';
import * as moment from 'moment';

import { injectable, ResourceService } from '../lib';
import { Mailer, Sequelize } from '../lib';
import { NoteTypeLabel, TypeLabel as OrderTypeLabel } from '../lib';


@injectable
export class NoteService extends ResourceService {

	constructor(
		private mailer: Mailer,
		private sequelize: Sequelize
	) {
		super(sequelize.models.note);
		mailer.register('note/note', null);
	}


	async send(id: number, selected: number[]) {
		const { models } = this.sequelize;

		const def = {
			model: models.event,
			attributes: ['id'],
			include: [
				{ model: models.case , attributes: ['id'  ] },
				{ model: models.study, attributes: ['id'  ] },
				{ model: models.order, attributes: ['type'] },
				{ model: models.user , attributes: ['id', 'email'], as: 'referringDoctor' },
				{ model: models.user , attributes: ['id', 'email'], as: 'dentist' },
			],
		};

		const note = await models.note.findById(id, {
			include: [def]
		});

		const { event } = note as any;

		const kase = await models.case.findById(event.case.id, {
			include: [
				{ model: models.patient, attributes: ['firstName', 'lastName'] },
			],
		});

		const { patient } = kase as any;

		const users = [
			event.referringDoctor,
			event.dentist,
		];

		const list = users
			.filter(user => user)
			.filter(user => selected.includes(user.id))
			.map(user => user.email);

		const recipients = lodash.uniq(list);

		const locals = {
			kase: {
				id: kase.id,
				name: patient.fullName,
			},
			event: {
				id: event.id,
				name: getEventName(event),
			},
			note: {
				name: getNoteName(note),
				date: moment.unix(note.date).format('L'),
			},
		};

		const subject = `${ locals.kase.name } » ${ locals.event.name } » ` +
			`${ locals.note.name } at ${ locals.note.date }`;

		this.mailer.send('note/note', recipients, locals, subject);
	}

}


function getEventName(event) {
	if (event.study) return 'BizMan (Sleep Study)';
	return OrderTypeLabel[event.order.type];
}


function getNoteName(note) {
	return NoteTypeLabel[note.type];
}
