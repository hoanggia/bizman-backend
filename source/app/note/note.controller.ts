import { parse, resource, route } from '../lib';
import { Context, ResourceController } from '../lib';

import { NoteService } from './note.service';


@resource('notes')
export class NoteController extends ResourceController {

	constructor(service: NoteService) {
		super(service);
	}


	@route.post('/notes/:id/send')
	async send(context: Context, id: string) {
		const selected = await parse(context);
		await (this.service as NoteService).send(+id, selected);
		context.status = 204; // No Content
	}

}
