"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
function default_1(sequelize) {
    const values = Object.keys(lib_2.NoteTypeList).map(key => lib_2.NoteTypeList[key]);
    sequelize.define('note', {
        files: {
            type: lib_1.Types.ARRAY(lib_1.Types.STRING),
            defaultValue: [],
        },
        type: {
            type: lib_1.Types.ENUM(values),
            allowNull: false,
        },
        date: {
            type: lib_1.Types.INTEGER,
            allowNull: false,
        },
        data: {
            type: lib_1.Types.JSONB,
            allowNull: false,
        },
    }, {
        timestamps: false,
        classMethods: { associate },
    });
}
exports.default = default_1;
function associate(models) {
    const { note } = models;
    note.belongsTo(models.event, {
        foreignKey: { allowNull: false },
        onDelete: 'cascade',
    });
    note.belongsTo(models.user, {
        as: 'createdBy',
        onDelete: 'restrict',
    });
    note.belongsTo(models.user, {
        as: 'updatedBy',
        onDelete: 'restrict',
    });
    note.belongsTo(models.user);
}
