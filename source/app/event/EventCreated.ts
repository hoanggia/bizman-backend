import { EventInstance } from 'persistence/event/event';


export class EventCreated {

	constructor(
		public readonly event: EventInstance,
		public readonly actorId: number,
	) {}

}
