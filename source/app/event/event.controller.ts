import { resource, ResourceController } from '../lib';

import { EventService } from './event.service';


@resource('events')
export class EventController extends ResourceController {

	constructor(service: EventService) {
		super(service);
	}

}
