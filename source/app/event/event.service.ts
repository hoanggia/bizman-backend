import { injectable, ResourceService, Sequelize } from '../lib';
import { NotFound, State, Mailer ,Config} from '../lib';

import { EventBus } from '../../system/EventBus';

import { EventCreated } from './EventCreated';
import { EventUpdated } from './EventUpdated';


@injectable
export class EventService extends ResourceService {

	constructor(
		private readonly bus: EventBus,
		private readonly state: State,
		private readonly sequelize: Sequelize,
		private mailer: Mailer,
	) {
		super(sequelize.models.event);
		mailer.register('event/request', 'You have a new case');
	}


	async create(data: any) {

		const { models } = this.sequelize;
		const { payload } = this.state;

		const item = await models.event.create(data);
		await this.bus.publish(new EventCreated(item, payload.id));

		return item;

	}


	async update(id: number, data: any) {

		const { models } = this.sequelize;
		const { payload } = this.state;

		const item = await models.event.findById(id);
		if (!item) throw new NotFound(id);

		if(item.type === 'study')
		{

			if(!item.referringDoctorId)
			{
				await item.update(data);
				await this.bus.publish(new EventUpdated(item, payload.id));
				const event = item;
				const specialist = await models.user.findById(item.reportingSpecialistId);
				this.mailer.send('event/request', specialist.email, { item, specialist });
				return;

			}
			else{
				await item.update(data);
				await this.bus.publish(new EventUpdated(item, payload.id));
				return;
			}
		}
		else
		{
			await item.update(data);
			await this.bus.publish(new EventUpdated(item, payload.id));
			return;
		}
	}


	async show(id: number) {

		const { models } = this.sequelize;

		const event = await models.event.findById(id, {
			include: [models.order, models.study, models.note, models.user],
		});

		return event;
	}

}
