"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const EventBus_1 = require("../../system/EventBus");
const EventCreated_1 = require("./EventCreated");
const EventUpdated_1 = require("./EventUpdated");
let EventService = class EventService extends lib_1.ResourceService {
    constructor(bus, state, sequelize, mailer) {
        super(sequelize.models.event);
        this.bus = bus;
        this.state = state;
        this.sequelize = sequelize;
        this.mailer = mailer;
        mailer.register('event/request', 'You have a new case');
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const item = yield models.event.create(data);
            yield this.bus.publish(new EventCreated_1.EventCreated(item, payload.id));
            return item;
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const item = yield models.event.findById(id);
            if (!item)
                throw new lib_2.NotFound(id);
            if (item.type === 'study') {
                if (!item.referringDoctorId) {
                    yield item.update(data);
                    yield this.bus.publish(new EventUpdated_1.EventUpdated(item, payload.id));
                    const event = item;
                    const specialist = yield models.user.findById(item.reportingSpecialistId);
                    this.mailer.send('event/request', specialist.email, { item, specialist });
                    return;
                }
                else {
                    yield item.update(data);
                    yield this.bus.publish(new EventUpdated_1.EventUpdated(item, payload.id));
                    return;
                }
            }
            else {
                yield item.update(data);
                yield this.bus.publish(new EventUpdated_1.EventUpdated(item, payload.id));
                return;
            }
        });
    }
    show(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const event = yield models.event.findById(id, {
                include: [models.order, models.study, models.note, models.user],
            });
            return event;
        });
    }
};
EventService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [EventBus_1.EventBus,
        lib_2.State,
        lib_1.Sequelize,
        lib_2.Mailer])
], EventService);
exports.EventService = EventService;
