import { EventInstance } from 'persistence/event/event';


export class EventUpdated {

	constructor(
		public readonly event: EventInstance,
		public readonly actorId: number,
	) {}

}
