"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const event_service_1 = require("./event.service");
let EventController = class EventController extends lib_1.ResourceController {
    constructor(service) {
        super(service);
    }
};
EventController = __decorate([
    lib_1.resource('events'),
    __metadata("design:paramtypes", [event_service_1.EventService])
], EventController);
exports.EventController = EventController;
