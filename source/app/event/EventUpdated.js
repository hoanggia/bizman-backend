"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EventUpdated {
    constructor(event, actorId) {
        this.event = event;
        this.actorId = actorId;
    }
}
exports.EventUpdated = EventUpdated;
