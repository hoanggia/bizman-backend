import { Models, Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('event', {

		files: {
			type: Types.ARRAY(Types.STRING),
			defaultValue: [],
		},

		date: {
			type: Types.INTEGER,
			allowNull: false,
		},

		type: {
			type: Types.STRING,
		},

		data: {
			type: Types.JSONB,
			allowNull: false,
		},

		completed: {
			type: Types.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

		reason: {
			type: Types.STRING,
		},

		reasonBy: {
			type: Types.JSONB,
		},
		isDeleted: {
			type: Types.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

	}, {

		timestamps: false,

		classMethods: { associate },

	});

}


function associate(models: Models) {

	const { event } = models;

	event.belongsTo(models.case, {
		foreignKey: { allowNull: false },
	});

	event.belongsTo(models.user, {
		as: 'referringDoctor',
		onDelete: 'restrict',
	});

	event.belongsTo(models.user, {
		as: 'dentist',
		onDelete: 'restrict',
	});

	event.belongsTo(models.user, {
		as: 'requestingSpecialist',
		onDelete: 'restrict',
	});

	event.belongsTo(models.user, {
		as: 'reportingSpecialist',
		onDelete: 'restrict',
	});

	event.belongsTo(models.user,{
		as: 'referringDoctorCPAP',
		onDelete: '',
	});

	event.belongsTo(models.user,{
		as: 'referringSleepCPAP',
		onDelete: '',
	});

	event.belongsTo(models.user, {
		as: 'updatedBy',
		onDelete: 'cascade',
		foreignKey: 'id',
	});

	event.belongsTo(models.user);


	event.hasOne(models.order, {
		foreignKey: { allowNull: false },
	});

	event.hasOne(models.study, {
		foreignKey: { allowNull: false },
	});

	event.hasMany(models.note, {
		foreignKey: { allowNull: false },
		onDelete: 'cascade',
	});

}
