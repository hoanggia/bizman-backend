"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('event', {
        files: {
            type: lib_1.Types.ARRAY(lib_1.Types.STRING),
            defaultValue: [],
        },
        date: {
            type: lib_1.Types.INTEGER,
            allowNull: false,
        },
        type: {
            type: lib_1.Types.STRING,
        },
        data: {
            type: lib_1.Types.JSONB,
            allowNull: false,
        },
        completed: {
            type: lib_1.Types.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        reason: {
            type: lib_1.Types.STRING,
        },
        reasonBy: {
            type: lib_1.Types.JSONB,
        },
        isDeleted: {
            type: lib_1.Types.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
    }, {
        timestamps: false,
        classMethods: { associate },
    });
}
exports.default = default_1;
function associate(models) {
    const { event } = models;
    event.belongsTo(models.case, {
        foreignKey: { allowNull: false },
    });
    event.belongsTo(models.user, {
        as: 'referringDoctor',
        onDelete: 'restrict',
    });
    event.belongsTo(models.user, {
        as: 'dentist',
        onDelete: 'restrict',
    });
    event.belongsTo(models.user, {
        as: 'requestingSpecialist',
        onDelete: 'restrict',
    });
    event.belongsTo(models.user, {
        as: 'reportingSpecialist',
        onDelete: 'restrict',
    });
    event.belongsTo(models.user, {
        as: 'referringDoctorCPAP',
        onDelete: '',
    });
    event.belongsTo(models.user, {
        as: 'referringSleepCPAP',
        onDelete: '',
    });
    event.belongsTo(models.user, {
        as: 'updatedBy',
        onDelete: 'cascade',
        foreignKey: 'id',
    });
    event.belongsTo(models.user);
    event.hasOne(models.order, {
        foreignKey: { allowNull: false },
    });
    event.hasOne(models.study, {
        foreignKey: { allowNull: false },
    });
    event.hasMany(models.note, {
        foreignKey: { allowNull: false },
        onDelete: 'cascade',
    });
}
