import { resource, ResourceController, RoleList } from '../lib';
import { KoaContext as Context } from 'koa';
import { json as parse } from 'co-body';
import { PatientService } from './patient.service';


const roleMap = {
	create: [RoleList.Admin, RoleList.Consultant, RoleList.ReferringDoctor, RoleList.SleepScientist],
	update: [RoleList.Admin, RoleList.Consultant, RoleList.ReferringDoctor, RoleList.SleepScientist],
	remove: [RoleList.Admin]
};


@resource('patients')
export class PatientController extends ResourceController {

	constructor(service: PatientService) {
		super(service);
	}

	protected getRoles(action: string) {
		return roleMap[action];
	}
	
}
