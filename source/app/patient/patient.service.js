"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
let PatientService = class PatientService extends lib_1.ResourceService {
    constructor(sequelize) {
        super(sequelize.models.patient);
        this.sequelize = sequelize;
    }
};
PatientService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize])
], PatientService);
exports.PatientService = PatientService;
