"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const patient_service_1 = require("./patient.service");
const roleMap = {
    create: [lib_1.RoleList.Admin, lib_1.RoleList.Consultant, lib_1.RoleList.ReferringDoctor, lib_1.RoleList.SleepScientist],
    update: [lib_1.RoleList.Admin, lib_1.RoleList.Consultant, lib_1.RoleList.ReferringDoctor, lib_1.RoleList.SleepScientist],
    remove: [lib_1.RoleList.Admin]
};
let PatientController = class PatientController extends lib_1.ResourceController {
    constructor(service) {
        super(service);
    }
    getRoles(action) {
        return roleMap[action];
    }
};
PatientController = __decorate([
    lib_1.resource('patients'),
    __metadata("design:paramtypes", [patient_service_1.PatientService])
], PatientController);
exports.PatientController = PatientController;
