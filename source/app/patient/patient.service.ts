import { injectable, ResourceService, Sequelize } from '../lib';


@injectable
export class PatientService extends ResourceService {

	constructor(
		private sequelize: Sequelize
	) {
		super(sequelize.models.patient);
	}

}
