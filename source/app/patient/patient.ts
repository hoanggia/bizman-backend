import * as moment from 'moment';

import { Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('patient', {

		firstName: {
			type: Types.STRING,
			allowNull: false,
		},

		lastName: {
			type: Types.STRING,
			allowNull: false,
		},

		medicareNo: {
			type: Types.STRING,
			unique: true,
		},

		gender: {
			type: Types.ENUM('male', 'female'),
		},

		dateOfBirth: {
			type: Types.BIGINT,
			allowNull: false,
		},

		address: {
			type: Types.JSONB,
			allowNull: false,
		},

		email: {
			type: Types.STRING,
		},

		phones: {
			type: Types.JSONB,
		},

	}, {

		timestamps: false,

		getterMethods: { fullName },

	});

}


function fullName() {
	return this.firstName + ' ' + this.lastName;
}
