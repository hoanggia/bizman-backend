"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('patient', {
        firstName: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
        lastName: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
        medicareNo: {
            type: lib_1.Types.STRING,
            unique: true,
        },
        gender: {
            type: lib_1.Types.ENUM('male', 'female'),
        },
        dateOfBirth: {
            type: lib_1.Types.BIGINT,
            allowNull: false,
        },
        address: {
            type: lib_1.Types.JSONB,
            allowNull: false,
        },
        email: {
            type: lib_1.Types.STRING,
        },
        phones: {
            type: lib_1.Types.JSONB,
        },
    }, {
        timestamps: false,
        getterMethods: { fullName },
    });
}
exports.default = default_1;
function fullName() {
    return this.firstName + ' ' + this.lastName;
}
