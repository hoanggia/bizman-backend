"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const report_service_1 = require("./report.service");
let ReportController = class ReportController {
    constructor(service) {
        this.service = service;
    }
    index(context) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.index();
        });
    }
};
__decorate([
    lib_1.authorize(),
    lib_1.route.get('/reports'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "index", null);
ReportController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [report_service_1.ReportService])
], ReportController);
exports.ReportController = ReportController;
