import { authorize, injectable, route } from '../lib';
import { Context } from '../lib';

import { ReportService } from './report.service';


@injectable
export class ReportController {

	constructor(private service: ReportService) {}


	@authorize()
	@route.get('/reports')
	async index(context: Context) {
		context.body = await this.service.index();
	}

}
