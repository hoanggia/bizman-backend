"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
let ProductService = class ProductService extends lib_1.ResourceService {
    constructor(sequelize) {
        super(sequelize.models.product);
        this.sequelize = sequelize;
    }
    index() {
        const { models } = this.sequelize;
        return models.product.findAll({ order: 'name' });
    }
};
ProductService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize])
], ProductService);
exports.ProductService = ProductService;
