"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const product_service_1 = require("./product.service");
const roleMap = {
    create: lib_1.RoleList.Admin,
    update: lib_1.RoleList.Admin,
    remove: lib_1.RoleList.Admin,
};
let ProductController = class ProductController extends lib_1.ResourceController {
    constructor(service) {
        super(service);
    }
    getRoles(action) {
        return roleMap[action];
    }
};
ProductController = __decorate([
    lib_1.resource('products'),
    __metadata("design:paramtypes", [product_service_1.ProductService])
], ProductController);
exports.ProductController = ProductController;
