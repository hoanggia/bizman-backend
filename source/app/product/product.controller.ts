import { resource, ResourceController, RoleList } from '../lib';

import { ProductService } from './product.service';


const roleMap = {
	create: RoleList.Admin,
	update: RoleList.Admin,
	remove: RoleList.Admin,
};


@resource('products')
export class ProductController extends ResourceController {

	constructor(service: ProductService) {
		super(service);
	}


	protected getRoles(action: string) {
		return roleMap[action];
	}

}
