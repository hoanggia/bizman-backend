import { Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('product', {

		sku: {
			type: Types.STRING,
			allowNull: false,
			unique: true,
		},

		name: {
			type: Types.STRING,
			allowNull: false,
		},

		price: {
			type: Types.INTEGER,
			allowNull: false,
		},

	}, {

		timestamps: false,

	});

}
