"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('product', {
        sku: {
            type: lib_1.Types.STRING,
            allowNull: false,
            unique: true,
        },
        name: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
        price: {
            type: lib_1.Types.INTEGER,
            allowNull: false,
        },
    }, {
        timestamps: false,
    });
}
exports.default = default_1;
