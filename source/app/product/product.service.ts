import { injectable, ResourceService, Sequelize } from '../lib';


@injectable
export class ProductService extends ResourceService {

	constructor(
		private readonly sequelize: Sequelize,
	) {
		super(sequelize.models.product);
	}


	index() {
		const { models } = this.sequelize;
		return models.product.findAll({ order: 'name' });
	}

}
