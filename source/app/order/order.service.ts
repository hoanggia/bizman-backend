import { injectable, ResourceService } from '../lib';
import { NotFound, Sequelize } from '../lib';


@injectable
export class OrderService extends ResourceService {

	constructor(private sequelize: Sequelize) {
		super(sequelize.models.order);
	}

}
