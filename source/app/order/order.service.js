"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
let OrderService = class OrderService extends lib_1.ResourceService {
    constructor(sequelize) {
        super(sequelize.models.order);
        this.sequelize = sequelize;
    }
};
OrderService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_2.Sequelize])
], OrderService);
exports.OrderService = OrderService;
