"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
function default_1(sequelize) {
    const values = Object.keys(lib_2.TypeList).map(key => lib_2.TypeList[key]);
    sequelize.define('order', {
        type: {
            type: lib_1.Types.ENUM(values),
            allowNull: false,
        },
    }, {
        timestamps: false,
    });
}
exports.default = default_1;
