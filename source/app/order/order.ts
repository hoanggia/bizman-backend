import { Models, Sequelize, Types } from '../lib';
import { TypeList } from '../lib';


export default function(sequelize: Sequelize) {

	const values = Object.keys(TypeList).map(key => TypeList[key]);

	sequelize.define('order', {

		type: {
			type: Types.ENUM(values),
			allowNull: false,
		},

	}, {

		timestamps: false,

	});

}
