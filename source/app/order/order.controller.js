"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const order_service_1 = require("./order.service");
let OrderController = class OrderController extends lib_2.ResourceController {
    constructor(service) {
        super(service);
    }
};
OrderController = __decorate([
    lib_1.resource('orders'),
    __metadata("design:paramtypes", [order_service_1.OrderService])
], OrderController);
exports.OrderController = OrderController;
