import { resource, route } from '../lib';
import { Context, ResourceController } from '../lib';

import { OrderService } from './order.service';


@resource('orders')
export class OrderController extends ResourceController {

	constructor(service: OrderService) {
		super(service);
	}

}
