"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
let RequestService = class RequestService {
    constructor(sequelize, state) {
        this.sequelize = sequelize;
        this.state = state;
        const { models } = sequelize;
        this.include = [
            {
                model: models.case,
                include: [models.patient],
            }, {
                model: models.study
            },
        ];
    }
    index() {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            return models.event.findAll({
                include: this.include,
                where: { reportingSpecialistId: payload.id },
            });
        });
    }
    show(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            return models.event.findById(id, {
                include: this.include,
                where: { reportingSpecialistId: payload.id },
            });
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const event = yield models.event.findById(id, {
                include: [models.study],
                where: { reportingSpecialistId: payload.id },
            });
            yield event['study'].update(data);
        });
    }
};
RequestService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize,
        lib_1.State])
], RequestService);
exports.RequestService = RequestService;
