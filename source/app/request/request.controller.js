"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const co_body_1 = require("co-body");
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const request_service_1 = require("./request.service");
let RequestController = class RequestController {
    constructor(service) {
        this.service = service;
    }
    index(context) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.index();
        });
    }
    show(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.show(+id);
        });
    }
    update(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield co_body_1.json(context);
            yield this.service.update(+id, data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.authorize(lib_2.RoleList.SleepSpecialist),
    lib_1.route.get('/requests'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RequestController.prototype, "index", null);
__decorate([
    lib_1.authorize(lib_2.RoleList.SleepSpecialist),
    lib_1.route.get('/requests/:id'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], RequestController.prototype, "show", null);
__decorate([
    lib_1.authorize(lib_2.RoleList.SleepSpecialist),
    lib_1.route.patch('/requests/:id'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], RequestController.prototype, "update", null);
RequestController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [request_service_1.RequestService])
], RequestController);
exports.RequestController = RequestController;
