import { injectable, Sequelize, State } from '../lib';


@injectable
export class RequestService {

	private include;


	constructor(
		private sequelize: Sequelize,
		private state: State
	) {
		const { models } = sequelize;

		this.include = [
			{
				model: models.case,
				include: [models.patient],
			}, {
				model: models.study
			},
		];
	}


	async index() {
		const { models } = this.sequelize;
		const { payload } = this.state;

		return models.event.findAll({
			include: this.include,
			where: { reportingSpecialistId: payload.id },
		});
	}


	async show(id: number) {
		const { models } = this.sequelize;
		const { payload } = this.state;

		return models.event.findById(id, {
			include: this.include,
			where: { reportingSpecialistId: payload.id },
		});
	}


	async update(id: number, data) {
		const { models } = this.sequelize;
		const { payload } = this.state;

		const event = await models.event.findById(id, {
			include: [models.study],
			where: { reportingSpecialistId: payload.id },
		});

		await event['study'].update(data);
	}

}
