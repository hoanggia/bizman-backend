import { json as parse } from 'co-body';

import { authorize, injectable, route } from '../lib';
import { Context, RoleList } from '../lib';

import { RequestService } from './request.service';


@injectable
export class RequestController {

	constructor(private service: RequestService) {}


	@authorize(RoleList.SleepSpecialist)
	@route.get('/requests')
	async index(context: Context) {
		context.body = await this.service.index();
	}


	@authorize(RoleList.SleepSpecialist)
	@route.get('/requests/:id')
	async show(context: Context, id: string) {
		context.body = await this.service.show(+id);
	}


	@authorize(RoleList.SleepSpecialist)
	@route.patch('/requests/:id')
	async update(context: Context, id: string) {
		const data = await parse(context);
		await this.service.update(+id, data);
		context.status = 204; // No Content
	}

}
