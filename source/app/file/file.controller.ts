import * as path from 'path';

import * as fs from 'fs-extra';
import * as formidable from 'formidable';

import { injectable, route } from '../lib';
import { Context, promisify } from '../lib';

import { FileService } from './file.service';


@injectable
export class FileController {

	constructor(private service: FileService) {}


	@route.post('/files')
	async create(context: Context) {

		const form = new formidable.IncomingForm();
		const parse = promisify(form.parse).bind(form);
		const [, data] = await parse(context.req);

		const instance = await this.service.create({ name: data.file.name });
		const filePath = getPath(instance.id);
		await promisify(fs.move)(data.file.path, filePath);

		context.set('Location', '/files/' + instance.id);
		context.status = 201; // Created

	}


	@route.get('/files/:id')
	async show(context: Context, id: string) {
		context.body = await this.service.show(id);
	}


	@route.get('/files/:id/view')
	async view(context: Context, id: string) {

		const instance = await this.service.show(id);
		const filePath = getPath(instance.id);

		context.type = path.extname(instance.name);
		context.body = fs.createReadStream(filePath);
		context.set('Cache-Control', 'max-age=31536000');

		context.set('BizMan-Name', instance.name);

	}

}


function getPath(id: string) {
	const dataDir = process.env.DATA_DIR;
	return path.join(dataDir, 'files', id);
}
