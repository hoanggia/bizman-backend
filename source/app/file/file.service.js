"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
let FileService = class FileService {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            return models.file.create(data);
        });
    }
    show(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const file = yield models.file.findById(id);
            if (!file)
                throw new lib_1.NotFound(id);
            return file;
        });
    }
};
FileService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize])
], FileService);
exports.FileService = FileService;
