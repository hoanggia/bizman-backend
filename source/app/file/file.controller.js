"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs-extra");
const formidable = require("formidable");
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const file_service_1 = require("./file.service");
let FileController = class FileController {
    constructor(service) {
        this.service = service;
    }
    create(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const form = new formidable.IncomingForm();
            const parse = lib_2.promisify(form.parse).bind(form);
            const [, data] = yield parse(context.req);
            const instance = yield this.service.create({ name: data.file.name });
            const filePath = getPath(instance.id);
            yield lib_2.promisify(fs.move)(data.file.path, filePath);
            context.set('Location', '/files/' + instance.id);
            context.status = 201; // Created
        });
    }
    show(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.show(id);
        });
    }
    view(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const instance = yield this.service.show(id);
            const filePath = getPath(instance.id);
            context.type = path.extname(instance.name);
            context.body = fs.createReadStream(filePath);
            context.set('Cache-Control', 'max-age=31536000');
            context.set('BizMan-Name', instance.name);
        });
    }
};
__decorate([
    lib_1.route.post('/files'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "create", null);
__decorate([
    lib_1.route.get('/files/:id'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "show", null);
__decorate([
    lib_1.route.get('/files/:id/view'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "view", null);
FileController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [file_service_1.FileService])
], FileController);
exports.FileController = FileController;
function getPath(id) {
    const dataDir = process.env.DATA_DIR;
    return path.join(dataDir, 'files', id);
}
