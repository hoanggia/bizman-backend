import { injectable, NotFound, Sequelize } from '../lib';


@injectable
export class FileService {

	constructor(private sequelize: Sequelize) {}


	async create(data) {
		const { models } = this.sequelize;

		return models.file.create(data);
	}


	async show(id: string) {
		const { models } = this.sequelize;

		const file = await models.file.findById(id);
		if (!file) throw new NotFound(id);

		return file;
	}

}
