import { Sequelize, Types } from '../lib';


export default function(sequelize: Sequelize) {

	sequelize.define('file', {

		id: {
			type: Types.UUID,
			defaultValue: Types.UUIDV1,
			primaryKey: true,
		},

		name: {
			type: Types.STRING,
			allowNull: false,
		},

	}, {

		timestamps: false,

	});

}
