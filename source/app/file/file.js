"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
function default_1(sequelize) {
    sequelize.define('file', {
        id: {
            type: lib_1.Types.UUID,
            defaultValue: lib_1.Types.UUIDV1,
            primaryKey: true,
        },
        name: {
            type: lib_1.Types.STRING,
            allowNull: false,
        },
    }, {
        timestamps: false,
    });
}
exports.default = default_1;
