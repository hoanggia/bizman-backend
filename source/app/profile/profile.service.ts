import { injectable, hash, verify } from '../lib';
import { Sequelize, State } from '../lib';


@injectable
export class ProfileService {

	constructor(
		private sequelize: Sequelize,
		private state: State
	) {}


	async show() {
		const { models } = this.sequelize;

		const { id } = this.state.payload;
		const user = await models.user.findById(id);

		return user;
	}


	async update(data) {
		const { models } = this.sequelize;

		const { id } = this.state.payload;
		const user = await models.user.findById(id, {
			attributes: <any>{ include: 'hash' },
		});
		if (data.current) {
			const match = await verify(user.hash, data.current);
			if (match) {
				data.hash = await hash(data.password);
			} else {
				throw 418;
			}
		}
		await user.update(data);
	}

}
