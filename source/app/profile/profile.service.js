"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
let ProfileService = class ProfileService {
    constructor(sequelize, state) {
        this.sequelize = sequelize;
        this.state = state;
    }
    show() {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { id } = this.state.payload;
            const user = yield models.user.findById(id);
            return user;
        });
    }
    update(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { id } = this.state.payload;
            const user = yield models.user.findById(id, {
                attributes: { include: 'hash' },
            });
            if (data.current) {
                const match = yield lib_1.verify(user.hash, data.current);
                if (match) {
                    data.hash = yield lib_1.hash(data.password);
                }
                else {
                    throw 418;
                }
            }
            yield user.update(data);
        });
    }
};
ProfileService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_2.Sequelize,
        lib_2.State])
], ProfileService);
exports.ProfileService = ProfileService;
