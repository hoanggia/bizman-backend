import { injectable, route } from '../lib';
import { Context, parse } from '../lib';

import { ProfileService } from './profile.service';


@injectable
export class ProfileController {

	constructor(private service: ProfileService) {}


	@route.get('/profile')
	async show(context: Context) {
		context.body = await this.service.show();
	}


	@route.patch('/profile')
	async update(context: Context) {
		const data = await parse(context);
		await this.service.update(data);
		context.status = 204; // No Content
	}

}
