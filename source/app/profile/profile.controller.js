"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const profile_service_1 = require("./profile.service");
let ProfileController = class ProfileController {
    constructor(service) {
        this.service = service;
    }
    show(context) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.show();
        });
    }
    update(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield lib_2.parse(context);
            yield this.service.update(data);
            context.status = 204; // No Content
        });
    }
};
__decorate([
    lib_1.route.get('/profile'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "show", null);
__decorate([
    lib_1.route.patch('/profile'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProfileController.prototype, "update", null);
ProfileController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [profile_service_1.ProfileService])
], ProfileController);
exports.ProfileController = ProfileController;
