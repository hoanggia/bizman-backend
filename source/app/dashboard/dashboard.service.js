"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const lib_2 = require("../lib");
const forbiddenRoles = [
    lib_2.RoleList.ReferringDoctor,
    lib_2.RoleList.SleepSpecialist,
    lib_2.RoleList.SleepAnalyst,
];
let DashboardService = class DashboardService {
    constructor(sequelize, state) {
        this.sequelize = sequelize;
        this.state = state;
    }
    index() {
        return __awaiter(this, void 0, void 0, function* () {
            const { models } = this.sequelize;
            const { payload } = this.state;
            const isForbidden = forbiddenRoles.includes(payload.role);
            if (isForbidden)
                return [];
            const whereCase = {};
            if (payload.role === lib_2.RoleList.Consultant) {
                whereCase.consultantId = payload.id;
            }
            const include = [
                {
                    model: models.case,
                    where: whereCase,
                    include: [models.patient],
                }, {
                    model: models.note,
                    order: [['date', 'DESC']],
                    limit: 1,
                }, {
                    model: models.study,
                }, {
                    model: models.order,
                }, {
                    model: models.user,
                },
            ];
            const where = {
                completed: false,
            };
            const order = [
                ['date', 'DESC'],
            ];
            return models.event.findAll({ include, order, where });
        });
    }
};
DashboardService = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [lib_1.Sequelize,
        lib_1.State])
], DashboardService);
exports.DashboardService = DashboardService;
