import * as lodash from 'lodash';

import { injectable, Sequelize, State } from '../lib';
import { RoleList, TypeList } from '../lib';


const forbiddenRoles = [
	RoleList.ReferringDoctor,
	RoleList.SleepSpecialist,
	RoleList.SleepAnalyst,
];


@injectable
export class DashboardService {

	constructor(
		private sequelize: Sequelize,
		private state: State
	) {}


	async index() {

		const { models } = this.sequelize;
		const { payload } = this.state;

		const isForbidden = forbiddenRoles.includes(payload.role);
		if (isForbidden) return [];

		const whereCase = <any>{};

		if (payload.role === RoleList.Consultant) {
			whereCase.consultantId = payload.id;
		}

		const include = [
			{
				model: models.case,
				where: whereCase,
				include: [models.patient],
			}, <any>{
				model: models.note,
				order: [['date', 'DESC']],
				limit: 1,
			}, {
				model: models.study,
			}, {
				model: models.order,
			},{
				model: models.user,
			},
		];

		const where = <any>{
			completed: false,
		};

		const order = [
			['date', 'DESC'],
		];

		return models.event.findAll({ include, order, where });

	}

}
