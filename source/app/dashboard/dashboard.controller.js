"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../lib");
const dashboard_service_1 = require("./dashboard.service");
let DashboardController = class DashboardController {
    constructor(service) {
        this.service = service;
    }
    index(context) {
        return __awaiter(this, void 0, void 0, function* () {
            context.body = yield this.service.index();
        });
    }
};
__decorate([
    lib_1.route.get('/dashboard'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DashboardController.prototype, "index", null);
DashboardController = __decorate([
    lib_1.injectable,
    __metadata("design:paramtypes", [dashboard_service_1.DashboardService])
], DashboardController);
exports.DashboardController = DashboardController;
