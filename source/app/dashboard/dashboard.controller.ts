import { injectable, route, Context } from '../lib';

import { DashboardService } from './dashboard.service';


@injectable
export class DashboardController {

	constructor(private service: DashboardService) {}


	@route.get('/dashboard')
	async index(context: Context) {
		context.body = await this.service.index();
	}

}
