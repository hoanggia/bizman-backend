import { json as parse } from 'co-body';
import { KoaContext as Context } from 'koa';

import { inputValidator } from './inputValidator'
import { Unauthorized } from './errors';
import { RESOURCE_NAME } from './metadata';
import { State } from './state';

interface IResourceService {
	index(): Promise<any[]>;
	create(data: any): Promise<any>;

	show  (id: number): Promise<any>;
	update(id: number, data: any): Promise<any>;
	remove(id: number): Promise<void>;
}


export class ResourceController {

	private resourceName: string;


	constructor(protected service: IResourceService) {
		this.resourceName = Reflect.getMetadata(RESOURCE_NAME, this.constructor);
	}


	protected getRoles(action: string) {
		return null;
	}


	private state = new State();


	private authorize(action: string) {

		const roles = this.getRoles(action);
		if (!roles) return; // All roles are allowed

		const array = Array.isArray(roles) ? roles : [roles];
		const { payload } = this.state;

		const allowed = array.includes(payload.role);
		if (!allowed) throw new Unauthorized();

	}


	async index(context: Context) {
		this.authorize('index');
		context.body = await this.service.index();
	}


	async create(context: Context) {

		this.authorize('create');
		let data = await parse(context);
		data = inputValidator(this.resourceName, data)
		const item = await this.service.create(data);
		const path = '/' + this.resourceName + '/' + item.id;

		context.set('location', path);
		context.status = 201; // Created

	}


	async show(context: Context, id: string) {
		this.authorize('show');
		context.body = await this.service.show(+id);
	}


	async update(context: Context, id: string) {

		this.authorize('update');
		let data = await parse(context);
		data = inputValidator(this.resourceName, data)
		await this.service.update(+id, data);
		context.status = 204; // No Content

	}


	async remove(context: Context, id: string) {

		this.authorize('remove');
		await this.service.remove(+id);
		context.status = 204; // No Content

	}

}
