export class Config {
	baseDir: string;
	jwt;
	mailer;
	sequelize;
	transport;
}
