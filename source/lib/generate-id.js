"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
function generateId() {
    const str = lodash.random(0, 65535).toString(16);
    return lodash.padStart(str, 4, '0');
}
exports.generateId = generateId;
