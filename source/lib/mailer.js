"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const jade = require("jade");
const mailer = require("nodemailer");
const di_1 = require("./di");
const config_1 = require("./config");
const log_1 = require("./log");
let Mailer = class Mailer {
    constructor(config, log) {
        this.config = config;
        this.log = log;
        this.templateMap = new Map();
        this.transporter = mailer.createTransport(config.transport);
    }
    register(name, subject) {
        const filePath = path.join(this.config.baseDir, name + '.jade');
        const jadeFn = jade.compileFile(filePath);
        const template = { subject, jadeFn };
        this.templateMap.set(name, template);
    }
    send(name, recipients, locals, subject = null) {
        const template = this.templateMap.get(name);
        const title = subject || template.subject;
        locals.siteUrl = this.config.mailer.siteUrl;
        const mail = {
            from: this.config.mailer.from,
            to: recipients,
            subject: '[BizMan] ' + title,
            html: template.jadeFn(locals),
        };
        this.transporter.sendMail(mail, (error, info) => {
            if (error) {
                this.log.warn(error);
            }
            else {
                this.log.debug(info);
            }
        });
    }
};
Mailer = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [config_1.Config,
        log_1.Log])
], Mailer);
exports.Mailer = Mailer;
