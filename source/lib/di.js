"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("angular2/src/core/di");
exports.provide = di_1.provide;
const classes = [];
function injectable(target) {
    classes.push(target);
}
exports.injectable = injectable;
function create(providers) {
    return di_1.Injector.resolveAndCreate(providers.concat(classes));
}
exports.create = create;
