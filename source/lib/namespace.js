"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const continuation_local_storage_1 = require("continuation-local-storage");
const namespace = continuation_local_storage_1.createNamespace('bizman');
function get(key) {
    return namespace.get(key);
}
exports.get = get;
function set(key, value) {
    namespace.set(key, value);
}
exports.set = set;
function run(fn) {
    return __awaiter(this, void 0, void 0, function* () {
        const context = namespace.createContext();
        try {
            namespace.enter(context);
            yield fn();
        }
        finally {
            namespace.exit(context);
        }
    });
}
exports.run = run;
