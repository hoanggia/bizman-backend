"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("./di");
const state_1 = require("./state");
let Log = class Log {
    constructor(state) {
        this.state = state;
    }
    log(level, message) {
        if (level === 'debug')
            level = 'log';
        console[level](message, '\n');
    }
    debug(message) {
        this.log('debug', message);
    }
    info(message) {
        this.log('info', message);
    }
    warn(message) {
        this.log('warn', message);
    }
    error(message) {
        this.log('error', message);
    }
};
Log = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [state_1.State])
], Log);
exports.Log = Log;
