import { injectable } from './di';
import { State } from './state';


@injectable
export class Log {

	constructor(private state: State) {}


	private log(level: string, message: any) {
		if (level === 'debug') level = 'log';
		console[level](message, '\n');
	}


	debug(message: any) {
		this.log('debug', message);
	}


	info(message: any) {
		this.log('info', message);
	}


	warn(message: any) {
		this.log('warn', message);
	}


	error(message: any) {
		this.log('error', message);
	}

}
