import { injectable } from './di';

import * as namespace from './namespace';


const PAYLOAD = 'payload';


@injectable
export class State {

	get payload() {
		return namespace.get(PAYLOAD);
	}


	set payload(value) {
		namespace.set(PAYLOAD, value);
	}


	get requestId(): string {
		return namespace.get('requestId');
	}

}
