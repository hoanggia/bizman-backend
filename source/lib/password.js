"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const create = require("credential");
function generate() {
    const buffer = crypto_1.randomBytes(8);
    const base64 = buffer.toString('base64');
    return base64.slice(0, -1);
}
exports.generate = generate;
const service = create({ work: .2 });
function hash(password) {
    return service.hash(password);
}
exports.hash = hash;
function verify(hash, password) {
    return service.verify(hash, password);
}
exports.verify = verify;
