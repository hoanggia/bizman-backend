import * as _ from 'sequelize';

import { NotFound } from './errors';


type Model = _.Model<any, any>;


export class ResourceService {

	constructor(private model: Model) {}


	async index() {
		return this.model.findAll();
	}

	async create(data) {
		return this.model.create(data);
	}


	async show(id: number) {
		const item = await this.model.findById(id);
		if (!item) throw new NotFound(id);
		return item;
	}


	async update(id: number, data: any) {
		const item = await this.model.findById(id);
		if (!item) throw new NotFound(id);
		await item.update(data);
	}


	async remove(id: number) {
		const item = await this.model.findById(id);
		if (!item) throw new NotFound(id);

		await item.destroy();
	}

}
