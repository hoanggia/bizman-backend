import { randomBytes } from 'crypto';

import * as create from 'credential';


export function generate() {
	const buffer = randomBytes(8);
	const base64 = buffer.toString('base64');
	return base64.slice(0, -1);
}


const service = create({ work: .2 });


export function hash(password: string) {
	return service.hash(password);
}


export function verify(hash: string, password: string) {
	return service.verify(hash, password);
}
