"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("./errors");
class ResourceService {
    constructor(model) {
        this.model = model;
    }
    index() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.model.findAll();
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.model.create(data);
        });
    }
    show(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = yield this.model.findById(id);
            if (!item)
                throw new errors_1.NotFound(id);
            return item;
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = yield this.model.findById(id);
            if (!item)
                throw new errors_1.NotFound(id);
            yield item.update(data);
        });
    }
    remove(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = yield this.model.findById(id);
            if (!item)
                throw new errors_1.NotFound(id);
            yield item.destroy();
        });
    }
}
exports.ResourceService = ResourceService;
