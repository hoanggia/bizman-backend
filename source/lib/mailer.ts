import * as path from 'path';

import * as jade from 'jade';
import * as mailer from 'nodemailer';

import { injectable } from './di';
import { Config } from './config';
import { Log } from './log';


interface Template {
	subject: string;
	jadeFn: jade.Fn;
}


@injectable
export class Mailer {

	private templateMap: Map<string, Template>;
	private transporter: mailer.Transporter;


	constructor(
		private config: Config,
		private log: Log
	) {
		this.templateMap = new Map();
		this.transporter = mailer.createTransport(config.transport);
	}


	register(name: string, subject: string) {

		const filePath = path.join(this.config.baseDir, name + '.jade');
		const jadeFn = jade.compileFile(filePath);

		const template = { subject, jadeFn };
		this.templateMap.set(name, template);

	}


	send(name: string, recipients: string | string[], locals, subject = null) {

		const template = this.templateMap.get(name);
		const title = subject || template.subject;
		locals.siteUrl = this.config.mailer.siteUrl;

		const mail = {
			from   : this.config.mailer.from,
			to     : recipients,
			subject: '[BizMan] ' + title,
			html   : template.jadeFn(locals),
		};

		this.transporter.sendMail(mail, (error, info) => {
			if (error) {
				this.log.warn(error);
			} else {
				this.log.debug(info);
			}
		});

	}

}
