import * as lodash from 'lodash';


export function generateId() {
	const str = lodash.random(0, 65535).toString(16);
	return lodash.padStart(str, 4, '0');
}
