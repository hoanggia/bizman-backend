"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("./di");
exports.RESOURCE_NAME = 'resource:name';
function resource(name) {
    return function (target) {
        Reflect.defineMetadata(exports.RESOURCE_NAME, name, target);
        return di_1.injectable(target);
    };
}
exports.resource = resource;
exports.route = {
    delete: make('delete'),
    patch: make('patch'),
    get: make('get'),
    post: make('post'),
    put: make('put'),
};
exports.ROUTE_LIST = 'route:list';
function make(verb) {
    return function (path) {
        return function (target, key) {
            const list = Reflect.getMetadata(exports.ROUTE_LIST, target) || [];
            list.push({ key, path, verb });
            Reflect.defineMetadata(exports.ROUTE_LIST, list, target);
        };
    };
}
