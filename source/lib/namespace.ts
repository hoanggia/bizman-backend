import { createNamespace } from 'continuation-local-storage';


const namespace = createNamespace('bizman');


export { get, set, run };


function get(key: string) {
	return namespace.get(key);
}


function set(key: string, value: any) {
	namespace.set(key, value);
}


type Fn = () => Promise<void>;

async function run(fn: Fn) {
	const context = namespace.createContext();
	try {
		namespace.enter(context);
		await fn();
	} finally {
		namespace.exit(context);
	}
}
