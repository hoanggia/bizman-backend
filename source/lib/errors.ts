class BaseError extends Error {

	get name() {
		return this.constructor.name;
	}

}


export class BadRequest extends BaseError {
}


export class Unauthorized extends BaseError {
}


export class Forbidden extends BaseError {
}


export class NotFound extends BaseError {

	constructor(id: number | string) {
		super(id + ' was not found.');
	}

}
