import { sign, verify } from 'jsonwebtoken';

import { promisify } from '../common/promisify';

import { Config } from './config';
import { injectable } from './di';


const verifyAsync: (token: string, secret) => Promise<any>
	= promisify(verify);


@injectable
export class Jwt {

	private secret;


	constructor(config: Config) {
		this.secret = config.jwt.secret;
	}


	sign(payload) {
		return sign(payload, this.secret);
	}


	async verify(token: string) {
		return verifyAsync(token, this.secret);
	}

}
