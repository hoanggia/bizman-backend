"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const promisify_1 = require("../common/promisify");
let filler;
try {
    filler = require('pdffiller');
}
catch (e) {
    if (e.code === 'MODULE_NOT_FOUND') {
        console.log('pdffiller is not available');
        filler = {};
    }
    else {
        throw e;
    }
}
const fillForm = promisify_1.promisify(filler.fillForm);
exports.fillForm = fillForm;
