"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const compose = require("koa-compose");
exports.compose = compose;
const ptr = require("path-to-regexp");
const metadata_1 = require("./metadata");
function create(controllers) {
    return controllers.map(function (ctrl) {
        const middlewares = [];
        const name = Reflect.getMetadata(metadata_1.RESOURCE_NAME, ctrl.constructor);
        if (name) {
            const list = resource(name, ctrl);
            middlewares.push(...list);
        }
        const list = Reflect.getMetadata(metadata_1.ROUTE_LIST, ctrl) || [];
        for (const item of list) {
            const fn = route(item.verb, item.path, ctrl, ctrl[item.key]);
            middlewares.push(fn);
        }
        return compose(middlewares);
    });
}
exports.create = create;
function resource(name, ctrl) {
    const basePath = '/' + name;
    const itemPath = basePath + '/:id';
    return [
        route('get', basePath, ctrl, ctrl.index),
        route('post', basePath, ctrl, ctrl.create),
        route('get', itemPath, ctrl, ctrl.show),
        route('patch', itemPath, ctrl, ctrl.update),
        route('delete', itemPath, ctrl, ctrl.remove),
    ];
}
function route(verb, path, ctrl, action) {
    verb = verb.toUpperCase();
    const re = ptr(path);
    return function (context, next) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!pass(context.method, verb)) {
                yield next();
                return;
            }
            const matches = re.exec(context.path);
            if (matches) {
                const args = matches.slice(1).map(decode);
                yield action.call(ctrl, context, ...args);
            }
            else {
                yield next();
            }
        });
    };
}
function pass(method, verb) {
    if (method === 'HEAD' && verb === 'GET')
        return true;
    if (method === verb)
        return true;
    return false;
}
function decode(value) {
    if (value)
        return decodeURIComponent(value);
}
