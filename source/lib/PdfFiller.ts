import { promisify } from '../common/promisify';

let filler;

try {
	filler = require('pdffiller');
} catch (e) {
	if (e.code === 'MODULE_NOT_FOUND') {
		console.log('pdffiller is not available');
		filler = {};
	} else {
		throw e;
	}
}

const fillForm: (sourcePath: string, destPath: string, fields: any) => Promise<{}>
	= promisify(filler.fillForm);

export { fillForm };
