import { injectable } from './di';


export const RESOURCE_NAME = 'resource:name';


export function resource(name: string): ClassDecorator {

	return function(target) {
		Reflect.defineMetadata(RESOURCE_NAME, name, target);
		return injectable(target);
	};

}


export const route = {
	delete: make('delete'),
	patch : make('patch' ),

	get : make('get' ),
	post: make('post'),
	put : make('put' ),
};


export const ROUTE_LIST = 'route:list';


function make(verb: string) {

	return function(path: string) {

		return function(target, key) {

			const list = Reflect.getMetadata(ROUTE_LIST, target) || [];
			list.push({ key, path, verb });
			Reflect.defineMetadata(ROUTE_LIST, list, target);

		};

	}

}
