"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const co_body_1 = require("co-body");
const inputValidator_1 = require("./inputValidator");
const errors_1 = require("./errors");
const metadata_1 = require("./metadata");
const state_1 = require("./state");
class ResourceController {
    constructor(service) {
        this.service = service;
        this.state = new state_1.State();
        this.resourceName = Reflect.getMetadata(metadata_1.RESOURCE_NAME, this.constructor);
    }
    getRoles(action) {
        return null;
    }
    authorize(action) {
        const roles = this.getRoles(action);
        if (!roles)
            return; // All roles are allowed
        const array = Array.isArray(roles) ? roles : [roles];
        const { payload } = this.state;
        const allowed = array.includes(payload.role);
        if (!allowed)
            throw new errors_1.Unauthorized();
    }
    index(context) {
        return __awaiter(this, void 0, void 0, function* () {
            this.authorize('index');
            context.body = yield this.service.index();
        });
    }
    create(context) {
        return __awaiter(this, void 0, void 0, function* () {
            this.authorize('create');
            let data = yield co_body_1.json(context);
            data = inputValidator_1.inputValidator(this.resourceName, data);
            const item = yield this.service.create(data);
            const path = '/' + this.resourceName + '/' + item.id;
            context.set('location', path);
            context.status = 201; // Created
        });
    }
    show(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            this.authorize('show');
            context.body = yield this.service.show(+id);
        });
    }
    update(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            this.authorize('update');
            let data = yield co_body_1.json(context);
            data = inputValidator_1.inputValidator(this.resourceName, data);
            yield this.service.update(+id, data);
            context.status = 204; // No Content
        });
    }
    remove(context, id) {
        return __awaiter(this, void 0, void 0, function* () {
            this.authorize('remove');
            yield this.service.remove(+id);
            context.status = 204; // No Content
        });
    }
}
exports.ResourceController = ResourceController;
