import { KoaFn as Middleware } from 'koa';

import * as compose from 'koa-compose';
import * as ptr from 'path-to-regexp';

import { RESOURCE_NAME, ROUTE_LIST } from './metadata';


export { compose, create };


function create(controllers: Object[]) {

	return controllers.map(function(ctrl) {

		const middlewares = [];

		const name = Reflect.getMetadata(RESOURCE_NAME, ctrl.constructor);
		if (name) {
			const list = resource(name, ctrl);
			middlewares.push(...list);
		}

		const list = Reflect.getMetadata(ROUTE_LIST, ctrl) || [];
		for (const item of list) {
			const fn = route(item.verb, item.path, ctrl, ctrl[item.key]);
			middlewares.push(fn);
		}

		return compose(middlewares);

	});

}


function resource(name: string, ctrl: any) {

	const basePath = '/' + name;
	const itemPath = basePath + '/:id';

	return [
		route('get'   , basePath, ctrl, ctrl.index ),
		route('post'  , basePath, ctrl, ctrl.create),

		route('get'   , itemPath, ctrl, ctrl.show  ),
		route('patch' , itemPath, ctrl, ctrl.update),
		route('delete', itemPath, ctrl, ctrl.remove),
	];

}


function route(verb: string, path: string, ctrl: any, action: Function): Middleware {

	verb = verb.toUpperCase();
	const re = ptr(path);

	return async function(context, next) {

		if (!pass(context.method, verb)) {
			await next();
			return;
		}

		const matches = re.exec(context.path);
		if (matches) {
			const args = matches.slice(1).map(decode);
			await action.call(ctrl, context, ...args);
		} else {
			await next();
		}

	};

}


function pass(method, verb) {
	if (method === 'HEAD' && verb === 'GET') return true;
	if (method === verb) return true;
	return false;
}


function decode(value) {
	if (value) return decodeURIComponent(value);
}
