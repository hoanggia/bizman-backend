"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("./di");
const generate_id_1 = require("./generate-id");
const errors_1 = require("./errors");
const log_1 = require("./log");
const namespace = require("./namespace");
let Handler = class Handler {
    constructor(log) {
        this.log = log;
    }
    runCls(context, next) {
        return __awaiter(this, void 0, void 0, function* () {
            yield namespace.run(next);
        });
    }
    error(context, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestId = generate_id_1.generateId();
            namespace.set('requestId', requestId);
            try {
                yield next();
            }
            catch (error) {
                const status = convert(error);
                if (status === 400)
                    context.body = error.errors;
                if (status !== 401)
                    this.log.error(error.stack);
                context.status = status;
            }
        });
    }
    get all() {
        return [
            this.runCls.bind(this),
            this.error.bind(this),
        ];
    }
};
Handler = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [log_1.Log])
], Handler);
exports.Handler = Handler;
function convert(error) {
    if (Number.isInteger(error))
        return error;
    if (error.name === 'SequelizeForeignKeyConstraintError')
        return 400;
    if (error.name === 'SequelizeUniqueConstraintError')
        return 400;
    if (error instanceof errors_1.BadRequest)
        return 400;
    if (error instanceof errors_1.Unauthorized)
        return 401;
    if (error instanceof errors_1.Forbidden)
        return 403;
    if (error instanceof errors_1.NotFound)
        return 404;
    return 500;
}
