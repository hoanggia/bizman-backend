"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
const promisify_1 = require("../common/promisify");
const config_1 = require("./config");
const di_1 = require("./di");
const verifyAsync = promisify_1.promisify(jsonwebtoken_1.verify);
let Jwt = class Jwt {
    constructor(config) {
        this.secret = config.jwt.secret;
    }
    sign(payload) {
        return jsonwebtoken_1.sign(payload, this.secret);
    }
    verify(token) {
        return __awaiter(this, void 0, void 0, function* () {
            return verifyAsync(token, this.secret);
        });
    }
};
Jwt = __decorate([
    di_1.injectable,
    __metadata("design:paramtypes", [config_1.Config])
], Jwt);
exports.Jwt = Jwt;
