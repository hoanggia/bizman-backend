import { injectable } from './di';
import { generateId } from './generate-id';

import { BadRequest, Unauthorized, Forbidden, NotFound } from './errors';
import { Log } from './log';

import * as namespace from './namespace';


@injectable
export class Handler {

	constructor(private log: Log) {}


	async runCls(context, next) {
		await namespace.run(next);
	}


	async error(context, next) {

		const requestId = generateId();
		namespace.set('requestId', requestId);

		try {
			await next();
		} catch (error) {
			const status = convert(error);
			if (status === 400) context.body = error.errors;
			if (status !== 401) this.log.error(error.stack);
			context.status = status;
		}

	}


	get all() {
		return [
			this.runCls.bind(this),
			this.error.bind(this),
		];
	}

}


function convert(error) {
	if (Number.isInteger(error)) return error;
	if (error.name === 'SequelizeForeignKeyConstraintError') return 400;
	if (error.name === 'SequelizeUniqueConstraintError') return 400;
	if (error instanceof BadRequest  ) return 400;
	if (error instanceof Unauthorized) return 401;
	if (error instanceof Forbidden   ) return 403;
	if (error instanceof NotFound    ) return 404;
	return 500;
}
