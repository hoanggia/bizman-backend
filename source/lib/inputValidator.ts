function allWordsCapitalize(str){
    if (typeof str === 'string') {
        str = str.toLowerCase();
        return str.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase();});
    } else {
        return ''
    }
}

function firstWordCapitalize(str){
    if (typeof str === 'string') {
        const sentences = str.split('.')
        if (sentences[sentences.length-1] === "" || sentences[sentences.length-1] === " ") sentences.pop();
        console.log(sentences)
        const validateStr = sentences.map((sentence) =>{
            const trimSentence = sentence.trim()
            return trimSentence[0].toUpperCase() + trimSentence.slice(1)
        })
        return validateStr.join('. ') + '.'
    } else {
        return ''
   }
}

function handlePatients(data) {
    data.firstName  = (data.firstName) ? allWordsCapitalize(data.firstName) : data.firstName;
    data.lastName   = (data.lastName) ? allWordsCapitalize(data.lastName) : data.lastName;
    data.email      = (data.email) ? data.email.toLowerCase() : data.email;

    if (data.address) {
        data.address.line = (data.address.line) ? allWordsCapitalize(data.address.line) : data.address.line;
        data.address.suburb = (data.address.suburb) ? allWordsCapitalize(data.address.suburb) : data.address.suburb;
        data.address.state = (data.address.state) ? allWordsCapitalize(data.address.state) : data.address.state;
    }
    return data
}

function handleUsers(data) {
    data.firstName  = (data.firstName)  ? data.firstName.toUpperCase()      : data.firstName;
    data.lastName   = (data.lastName)   ? data.lastName.toUpperCase()       : data.lastName;
    data.email      = (data.email)      ? data.email.toLowerCase()          : data.email;
    data.address    = (data.address)    ? allWordsCapitalize(data.address)  : data.address;
    if (data.address2) {
        data.address2.address = (data.address2.address) ? allWordsCapitalize(data.address2.address) : data.address2.address;
        data.address2.suburb = (data.address2.suburb) ? allWordsCapitalize(data.address2.suburb) : data.address2.suburb;
        data.address2.state = (data.address2.state) ? allWordsCapitalize(data.address2.state) : data.address2.state;
    }
    return data
}

function handleProducts(data) {
    data.name = (data.name) ? allWordsCapitalize(data.name) : data.name;
    return data
}

function handleEvents(data) {
    if (data.data.clinical) {
        data.data.clinical.reason = (data.data.clinical.reason) ? firstWordCapitalize(data.data.clinical.reason) : data.data.clinical.reason;
        data.data.clinical.details = (data.data.clinical.details) ? firstWordCapitalize(data.data.clinical.details) : data.data.clinical.details;
    }
    return data
}

function handleStudies(data) {
    if (data.studyAppointment) {
        data.studyAppointment.technician = (data.studyAppointment.technician) ? firstWordCapitalize(data.studyAppointment.technician) : data.studyAppointment.technician;
        data.studyAppointment.pdx = (data.studyAppointment.pdx) ? firstWordCapitalize(data.studyAppointment.pdx) : data.studyAppointment.pdx;
        data.studyAppointment.scientist = (data.studyAppointment.scientist) ? firstWordCapitalize(data.studyAppointment.scientist) : data.studyAppointment.scientist;
        data.studyAppointment.comments = (data.studyAppointment.comments) ? firstWordCapitalize(data.studyAppointment.comments) : data.studyAppointment.comments;
        data.studyAppointment.solutions = (data.studyAppointment.solutions) ? firstWordCapitalize(data.studyAppointment.solutions) : data.studyAppointment.solutions;
        data.studyAppointment.deviceError = (data.studyAppointment.deviceError) ? firstWordCapitalize(data.studyAppointment.deviceError) : data.studyAppointment.deviceError;
        data.studyAppointment.deviceErrorSolution = (data.studyAppointment.deviceErrorSolution) ? firstWordCapitalize(data.studyAppointment.deviceErrorSolution) : data.studyAppointment.deviceErrorSolution;
        data.studyAppointment.deviceErrorComment = (data.studyAppointment.deviceErrorComment) ? firstWordCapitalize(data.studyAppointment.deviceErrorComment) : data.studyAppointment.deviceErrorComment;
    }
    return data
}

export function inputValidator(resource, data) {
    console.log("Resource: ", resource)
    console.log("Data: ", data)
    switch (resource) {
        case 'patients':
            return handlePatients(data)
        case 'users':
            return handleUsers(data)
        case 'products':
            return handleProducts(data)
        case 'events':
            return handleEvents(data)
        case 'studies':
            return handleStudies(data)
        default:
            return data
    }
}