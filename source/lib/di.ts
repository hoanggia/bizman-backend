import { Injector, provide } from 'angular2/src/core/di';


const classes = [];


export function injectable(target) {
	classes.push(target);
}


export { provide };


export function create(providers: any[]) {
	return Injector.resolveAndCreate(providers.concat(classes));
}
