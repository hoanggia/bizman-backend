"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const di_1 = require("./di");
const namespace = require("./namespace");
const PAYLOAD = 'payload';
let State = class State {
    get payload() {
        return namespace.get(PAYLOAD);
    }
    set payload(value) {
        namespace.set(PAYLOAD, value);
    }
    get requestId() {
        return namespace.get('requestId');
    }
};
State = __decorate([
    di_1.injectable
], State);
exports.State = State;
