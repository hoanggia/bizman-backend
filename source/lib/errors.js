"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseError extends Error {
    get name() {
        return this.constructor.name;
    }
}
class BadRequest extends BaseError {
}
exports.BadRequest = BadRequest;
class Unauthorized extends BaseError {
}
exports.Unauthorized = Unauthorized;
class Forbidden extends BaseError {
}
exports.Forbidden = Forbidden;
class NotFound extends BaseError {
    constructor(id) {
        super(id + ' was not found.');
    }
}
exports.NotFound = NotFound;
