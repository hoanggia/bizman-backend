declare module 'koa-compose' {

	import { KoaFn as Middleware } from 'koa';

	function compose(middlewares: Middleware[]): Middleware;

	module compose {}

	export = compose;

}
