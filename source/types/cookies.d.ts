declare module 'cookies' {

	class Cookies {
		get(name: string): string;
		set(name: string, value?: string, options?): Cookies;
	}

	module Cookies {}

	export = Cookies;

}