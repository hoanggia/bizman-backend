declare module 'pdffiller' {

	function fillForm(sourcePath: string, destPath: string, fields: any, callback: Function): void;

}
