declare module 'angular2/src/core/di' {

	class Injector {
		static resolveAndCreate(providers: any[]): Injector;
		get(token: any): any;
	}

	class Provider {}

	interface Options {
		deps?: Object[];
		useFactory?: Function;
		useValue?: any;
	}

	function provide(token: any, options: Options): Provider;

}
