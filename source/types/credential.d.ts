declare module 'credential' {

	interface Credential {
		hash(password: string): Promise<string>;
		verify(hash: string, password: string): Promise<boolean>;
	}

	interface OptionDict {
		work: number;
	}

	function create(options?: OptionDict): Credential;

	module create {}

	export = create;

}
