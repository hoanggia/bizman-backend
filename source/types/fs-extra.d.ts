declare module 'fs-extra' {

	export * from 'fs';

	export function move(src: string, dest: string, callback: (err) => void): void;

}
