declare module 'koa' {

	import * as http from 'http';
	import * as events from 'events';

	import * as Cookies from 'cookies';


	class Application extends events.EventEmitter {
		use(fn: Middleware): Application;
	}


	interface Context {
		req: http.IncomingMessage;
		res: http.ServerResponse;

		request: Request;
		response: Response;

		cookies: Cookies;

		// Request
		get(name: string): string;
		method: string;
		path: string;
		query;

		// Response
		body;
		attachment(name: string): void;
		set(name: string, value: string): void;
		status: number;
		type: string;
	}


	interface Request {
		get(name: string): string;
		method: string;
		path: string;
		query;
	}


	interface Response {
		body;
		set(name: string, value: string): void;
		status: number;
		type: string;
	}


	type Next = () => Promise<void>;

	type Middleware = (context: Context, next: Next) => Promise<void>;


	module Application {
		export type KoaContext = Context;
		export type KoaFn = Middleware;
	}

	export = Application;

}
