declare module 'jade' {

	type Fn = (locals?: any) => string;

	function compileFile(path: string): Fn;

}
