declare module 'continuation-local-storage' {

	interface Namespace {
		createContext();

		enter(context);
		exit (context);

		get(key: string): any;
		set(key: string, value: any): void;
	}

	function createNamespace(name: string): Namespace;
	function    getNamespace(name: string): Namespace;

}
