"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const config_1 = require("./lib/config");
const Config_1 = require("./system/Config");
const di_1 = require("./lib/di");
const schema_1 = require("./config/schema");
const router_1 = require("./config/router");
const Components_1 = require("./components/Components");
const Router_1 = require("./system/Router");
function start(config) {
    const injector = di_1.create([
        di_1.provide(config_1.Config, { useValue: config }),
        di_1.provide(Config_1.Config, { useValue: config }),
    ]);
    const schema = injector.get(schema_1.Schema);
    const router = injector.get(router_1.Router);
    const components = injector.get(Components_1.Components);
    const myRouter = injector.get(Router_1.Router);
    const app = new Koa();
    app.use(router.compose());
    app.use(myRouter.middleware());
    console.log(schema);
    return schema.sync().then(() => app);
}
exports.start = start;
