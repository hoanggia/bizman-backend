'use strict';

require('reflect-metadata');
require('./helpers/typescript');

const path = require('path');
const configFile = path.join(process.env.DATA_DIR, 'config');

const
	config = require(configFile),
	source = require('./source');

config.baseDir = path.join(__dirname, 'source/app');

config.source = {
	path: path.join(__dirname, 'source'),
};

source.start(config).then(listen, handler);

function listen(koa) {
	const server = koa.listen(config.listen);
	server.on('listening', function() {
		console.log(server.address());
	});
	server.on('error', handler);
}

function handler(error) {
	console.error(error.stack);
	process.exitCode = 1;
}
