Bizman v2 backend source code:

+Steps to manually build code for deploy:
    tsc -p ./source (--watch to detect any changes in typescript source)

+ENV config. This config file should locate at '../data' for bizman v2 to read:

    module.exports = {
        sequelize: {
            database: '',
            username: '',
            password: '',
            options : {},
        },
        transport: {
            host: 'smtp.mailgun.org',
            auth: {
                user: '',
                pass: '',
            },
            secure: true,
        },

        mailer: {
            from: '',
            siteUrl: '',
        },
        jwt: {
            secret: '',
        },

        listen: {
            port: 3004,
        },

        source: {
            path: ''
        }
    };

+Run command:
    pm2 start apps.yaml           : Just start bizman v2
    pm2 start apps.yaml && pm2 log: Start bizman v2 and output pm2 log to screen

+P/S:
    Any file uploaded by users will be stored at '../data/files'
