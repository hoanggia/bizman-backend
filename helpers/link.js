'use strict';

const
	fs = require('fs'),
	path = require('path');

const DIST_PATH = path.join(__dirname, '..', '.dist', 'server');

function run(fn) {
	try {
		fn();
	} catch (error) {
		if (error.code !== 'EEXIST') throw error;
	}
}

run(() => fs.mkdirSync('.dist'));
run(() => fs.mkdirSync(DIST_PATH));
run(() => fs.symlinkSync(
	path.join(DIST_PATH, 'source'),
	path.join(DIST_PATH, 'node_modules'),
	'junction'
));
